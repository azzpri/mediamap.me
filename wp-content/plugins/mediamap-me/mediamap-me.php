<?php
/*
Plugin Name: Mediamap-Me
Plugin URI: http://mediamap.me
Description: Personal Plugin
Version: 1.0.0
Author: Sergey Vosrikov
Author URI: sergey.vostrikov@outlook.com
*/


define('MEDIAMAP_DIR', plugin_dir_path(__FILE__));
define('MEDIAMAP_URL', plugin_dir_url(__FILE__));

function mediamap_meInit(){
 
    if(is_admin()) { // подключаем файлы администратора, только если он авторизован
        require_once(MEDIAMAP_DIR.'includes/admin/index.php');        
    }
 
    require_once(MEDIAMAP_DIR.'includes/core.php');
}
mediamap_meInit();

register_activation_hook(__FILE__, 'mediamap_meActivation');
 
function mediamap_meActivation() {
 
    // действие при активации
    
    // регистрируем действие при удалении
    register_uninstall_hook(__FILE__, 'mediamap_meUninstall');
}
 
function mediamap_meUninstall(){

    //действие при удалении
}

/**
 * Ensure user has valid user login
 *
 * When an user tries to login with a Social provider
 * and his name is non-latin,
 * Wordpress social login triggers the following error message:
 * 'An error occurred while creating a new user: Cannot create a user with an empty login name'.
 * So if the user doesn't have a user login, we use email's username instead it.
 *
 * @param array $user_data
 *
 * @return array $user_data
 */
function ensure_user_login( $user_data ) {
    $user_login = $user_data['user_login'];
    $user_email = $user_data['user_email'];

    if ( empty( $user_login ) ) {
        // There isn't $user_login,
        // so use email's username as user login
        $user_login              = sanitize_user( current( explode( '@', $user_email ) ), true );
        $user_data['user_login'] = $user_login;
    }

    return $user_data;
}

add_filter( 'wsl_hook_process_login_alter_wp_insert_user_data',  'ensure_user_login' );

?>