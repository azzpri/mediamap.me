<?php

require_once(MEDIAMAP_DIR.'includes/admin/model.php');

add_action('wp_ajax_nopriv_getElements', 'getElements_callback');
add_action('wp_ajax_nopriv_userState', 'userState_callback');
add_action('wp_ajax_nopriv_getCards', 'getCards_callback');
add_action('wp_ajax_nopriv_sendEmail', 'sendEmail_callback');

add_action('wp_ajax_getElements', 'getElements_callback');
add_action('wp_ajax_userState', 'userState_callback');
add_action('wp_ajax_getCards', 'getCards_callback');
add_action('wp_ajax_updateElements', 'updateElements_callback');
add_action('wp_ajax_cards', 'cards_callback');
add_action('wp_ajax_sendEmail', 'sendEmail_callback');
add_action('wp_ajax_pdf', 'pdf_callback');

function getCards_callback() {
	$data = file_get_contents("php://input");
	$post_data = json_decode( $data );
	
	if( count( $post_data ) > 0 ){
		
		global $Model;
		
		if ( isset( $post_data->element_id ) ){
			if ( isset( $post_data->only ) ){
				print_r( json_encode( $Model->Cards->getCardsForElementOnly( $post_data->element_id ) ) );	
			} else {
				print_r( json_encode( $Model->Cards->getCardsForElement( $post_data->element_id ) ) );	
			}
			wp_die();
		}		
	}
	
	echo 0;
	wp_die();
}

function generateRandomString($length = 6) {
      return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }

/*
 *	Create Card Tree
 */
function createCardTree( $Model, $user_id, $type ) {
	$elements = $Model->Elements->getElements();
	$user_elements = $Model->UsersElements->getUserElements( $user_id, $type );

	$tree = array();
	
	foreach ( $user_elements as $user_element ) {
		
		// find user element in elements
		foreach ( $elements as $element ) {
			
			if ( $element->id == $user_element->element_id ) {
				$user_element_data = $element;
				break;
			}
			
		}
		
		// get element tree				
		$branch = array( $user_element_data );

		while ( true ) {
			
			if ( $branch[ 0 ]->parent_id == 0 )
				break;
			
			foreach ( $elements as $element ) {
				if ( $element->id == $branch[ 0 ]->parent_id ) {
					array_unshift( $branch, $element );
					break;
				}
			}
		}
										
		// add element branch to tree
		$search_in =& $tree;
		
		foreach ( $branch as $branch_element ) {
			
			$add_element = true;
													
			for ( $i = 0; $i < count( $search_in ); $i ++ ) {
				
				if ( $search_in[ $i ]['element']->id == $branch_element->id ) {
					$add_element = false;
					$search_in =& $search_in[ $i ]['children'];							
					break;
				}
				
			}
			
			if ( $add_element ) {
				
				$search_in[] = array( "element" => $branch_element, 'children' => array() );																		
				$search_in =& $search_in[ count( $search_in ) - 1 ]['children']; 						
			} 
			
		}
		
	}
	
	return $tree;
}
			
function pdf_callback() {
	$data = file_get_contents("php://input");
	$post_data = json_decode( $data );
			
	if ( count( $post_data ) > 0 and is_user_logged_in()  ){
		
		global $Model;
		
		if ( isset( $post_data->type ) ){
			
			require_once( MEDIAMAP_DIR.'includes/tcpdf/tcpdf.php' );			

			$type = "Я хочу научиться:";
			
			if ( $post_data->type == 1 )
				$type = "Я умею:";
			
			$user_id = wp_get_current_user()->data->ID;
			$user_cards = $Model->Cards->getUserCards( $user_id, $post_data->type );
			
			$upload_dir_path = wp_upload_dir();
			$file_name = generateRandomString() . ".pdf";
																		
			$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);
			
			// get Card Tree
			$tree = createCardTree( $Model, $user_id, $post_data->type );
			
			$path = '';
			$mailText = '';
					
			// prepair mailText						
			function mailText_rec( $search_in, &$path, &$mailText, $user_cards, $pdf ) {
				
				foreach ( $search_in as $element ) {
					
					if ( count( $element['children'] ) > 0 ){
						
						// add element name as part of path
						if ( strlen( $path ) > 0 )						
							$path .= " > ";
							
						$path .= $element['element']->name_rus;						
						
						// go down element and run function again
						mailText_rec( $element['children'], $path, $mailText, $user_cards, $pdf );
					} else {
						
						/*	 Path is over. Print it and add element information
						 */
						
						// add path
						if ( strlen( $path ) > 0 ) {
																					
							$html = '<br><br><div style="color:rgb(16, 165, 228);font-size:13px;">' . $path . "</div>";
							$pdf->writeHTML($html, true, false, true, false, '');
						}
							
						// clear path for next element
						$path = '';
						
						// add element name
						$html = '<div style="font-size:13px;">&nbsp;&nbsp;' . $element[ 'element' ]->name_rus . "</div>";
						$pdf->writeHTML($html, true, false, true, false, '');
							
						// add card information
						$card_text = '';
												
						foreach ( $user_cards as $user_card ) {
							
							if ( $user_card->element_id == $element[ 'element' ]->id ) {
																
								$card_text .= "<ol>";
								
								// add action
								if( strlen( $user_card->action_rus ) > 0 )								
									$card_text .= $user_card->action_rus . " : ";
																
								// add link
								$link = "http://mediamap.me?element=". $user_card->element_id ."&card=". $user_card->id;
								$card_text .= '<a href="'.$link.'">' . $user_card->name_rus ."</a></ol>";
							}
						}
						
						// if no card add information
						if ( strlen( $card_text ) == 0 )
							$card_text = "<ol>нет карточек</ol>";
													
						//$html = '<li>' . $card_text . "</li>";
						$html = $card_text;
						$pdf->writeHTML($html, true, false, true, false, '');						
																								
					}					
				}				
			}
			
			$pdf->AddPage(); // создаем первую страницу, на которой будет содержимое
			$pdf->setFontSubsetting(true);			
			$pdf->SetFont('freeserif', '', 11);
			
			$html = '<h2>MediaMap.me - интерактивная карта медийных компенций</h2>';
			$html .= '<h1>'. $type. '</h1>';
			$pdf->writeHTML($html, true, false, true, false, '');						
			
			mailText_rec( $tree, $path, $mailText, $user_cards, $pdf );
			
			//
			//$pdf->SetMargins(20, 25, 25); // устанавливаем отступы (20 мм - слева, 25 мм - сверху, 25 мм - справа)			
			//$pdf->SetXY(90, 10); // устанавливаем координаты вывода текста в рамке: // 90 мм - отступ от левого края бумаги, 10 мм - от верхнего
			//$pdf->SetDrawColor(0, 0, 200); // устанавливаем цвет рамки (синий)
			//$pdf->SetTextColor(0, 200, 0); // устанавливаем цвет текста (зеленый)			
			//$pdf->setFontSubsetting(true);			
			//$pdf->SetFont('freeserif', '', 9);
			//$pdf->Cell(30, 6, 'Привет, Мир!', 1, 1, 'C'); // выводим ячейку с надписью шириной 30 мм и высотой 6 мм. Строка отцентрирована относительно границ ячейки
			$pdf->Output( $upload_dir_path[ "path" ] ."/". $file_name , 'F'); // выводим документ в браузер, заставляя его вклю	
			
			echo $upload_dir_path[ "url" ] ."/". $file_name;			
		}
				
		wp_die();
	}
	
	echo 0;
	wp_die();
}

function sendEmail_callback() {
	$data = file_get_contents("php://input");
	$post_data = json_decode( $data );
			
	if ( count( $post_data ) > 0 and is_user_logged_in()  ){
		
		global $Model;
		
		if ( isset( $post_data->type ) ){
			
			$type = "Хочу научиться";
			
			if ( $post_data->type == 1 )
				$type = "Умею";			
			
			$user_id = wp_get_current_user()->data->ID;						
			$user_cards = $Model->Cards->getUserCards( $user_id, $post_data->type );
			
			$tree = createCardTree( $Model, $user_id, $post_data->type );
									
			$path = '';
			$mailText = '';
					
			// prepair mailText						
			function mailText_rec( $search_in, &$path, &$mailText, $user_cards ) {
				
				foreach ( $search_in as $element ) {
					
					if ( count( $element['children'] ) > 0 ){
						
						// add element name as part of path
						if ( strlen( $path ) > 0 )						
							$path .= " > ";
							
						$path .= $element['element']->name_rus;						
						
						// go down element and run function again
						mailText_rec( $element['children'], $path, $mailText, $user_cards );
					} else {
						
						/*	 Path is over. Print it and add element information
						 */
						
						// add path
						if ( strlen( $path ) > 0 ) {
							
							if( strlen( $mailText ) > 0 )
								$mailText .= "<br>";
								
							$mailText .= "<div>" .$path . "</div>";
							
						}
							
						// clear path for next element
						$path = '';
						
						// add element name
						$mailText .= "<div> - " . $element[ 'element' ]->name_rus . "</div>";
						
						// add card information
						$card_text = '';
												
						foreach ( $user_cards as $user_card ) {
							
							if ( $user_card->element_id == $element[ 'element' ]->id ) {
								
								$card_text .= '<div>&nbsp;&nbsp;-&nbsp;';
								
								// add action
								if( strlen( $user_card->action_rus ) > 0 ) {								
									$card_text .= $user_card->action_rus . " ";
								} else if( strlen( $user_card->action_eng ) > 0 ) {								
									$card_text .= $user_card->action_eng . " ";
								}
								
								// add name									
								$card_text .= '<a href="http://mediamap.me?element='. $user_card->element_id .'&card='. $user_card->id .'">';
								$card_text .= strlen( $user_card->name_rus ) > 0 ? $user_card->name_rus : $user_card->name_eng  . "</a></div>";
							}
						}
						
						// if no card add information
						if ( strlen( $card_text ) == 0 )
							$card_text .= '<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;нет карточек;</div>';
						
						$mailText .= "<p>" . $card_text ."</p>";
					}					
				}				
			}
			
			mailText_rec( $tree, $path, $mailText, $user_cards );
			
			$text = "<!DOCTYPE html><html><body><p>Привет!</p>";
			
			$text .= "<p>Рады видеть тебя на mediamap.me. В этом письме — список того, что ты уже умеешь в мире медиа, и материалы, которые помогут овладеть этими компетенциями еще лучше. Полезного чтения!</p><p><b>Твои медиакомпетенции:</b></p>";
			
			if ( $type == "Хочу научиться" )
				$text = "<p>Рады видеть тебя на mediamap.me. В этом письме — список навыков в мире медиа, которыми ты хочешь овладеть, и материалы, которые помогут тебе достичь этой цели. Надеемся, что уже скоро ты отметишь эти компетенции как изученные ;)</p><p>Полезного чтения!</p><p><b>Твои будущие навыки:</b></p>";
										
			add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
			
			$result = wp_mail(
				wp_get_current_user()->data->user_email,
				"Карта твоих медиакомпетенций",
				$text . $mailText . '<p align="right">Успеха в профессиональном росте,</p><p align="right">Mediamap.me</p></body></html>'
			);
			
			echo 1;
			wp_die();
		}		
	}
	
	echo 0;
	wp_die();
}

function cards_callback() {
	$data = file_get_contents("php://input");
	$post_data = json_decode( $data );
	
	if( count( $post_data ) > 0 && isset( $post_data->action ) ){
		
		global $Model;
		
		if ( $post_data->action == 'deleteCard' && isset( $post_data->card_id ) ){
			$Model->Cards->removeCard( $post_data->card_id );
		}
		
		if ( $post_data->action == 'updateCards' && isset( $post_data->data ) ){
						
			foreach( $post_data->data as $dt ) {
				
				$card_id = $dt->id;
				
				if( isset( $dt->action ) ) {
					if( $dt->action == 'add' )
						$card_id = $Model->Cards->addCard( $dt );
					if( $dt->action == 'edit' )
						$Model->Cards->updateCard( $dt );
				}
				
				if( $dt->check == "check" ) {
					$Model->Cards->addElementCard( $post_data->element_id, $dt->id );
				}
				
				if( $dt->check == "remove_check" ) {
					$Model->Cards->removeElementCard( $post_data->element_id, $dt->id );
				}
			}
		}					
		echo 1;
		wp_die();
	}
	
	echo 0;
	wp_die();
}

function userState_callback() {
	$data = file_get_contents("php://input");
	$post_data = json_decode( $data );
	
	if( count( $post_data ) > 0 and is_user_logged_in()  ){
		
		global $Model;
		
		$user_id = wp_get_current_user()->data->ID;
		$remove_array = array();
		$insert_array = array();
		
		foreach( $post_data as $pd ) {
			$remove_array[] = $pd->id;
			if( $pd->state > 0 )
				$insert_array[] = array( $user_id, $pd->id, $pd->state);
		}
		
		if( count( $remove_array ) > 0 ) {
			$Model->UsersElements->removeUserElements( $user_id, $remove_array );
		}
		
		if( count( $insert_array ) > 0 ) {
			$Model->UsersElements->addUserElements( $user_id, $insert_array );
		}
		
		echo 1;
		wp_die();
	}
	
	echo 0;
	wp_die();
	
}

function getElements_callback() {
	
	global $Model;
		
	print_r( json_encode( $Model->Elements->getElements() ) );	
	wp_die();
}
		
function updateElements_callback( ) {
	
	$data = file_get_contents("php://input");
	$post_data = json_decode( $data );
		
	if( count( $post_data ) == 0 ) {
		echo 0;
		wp_die();
	}
	
	global $Model;
	
	$insert_array = array();
	$update_array = array();
	$not_delete_array = array();
	
	// Save Screen Size
	$elements = $Model->Elements->getElements();
	if( count( $elements ) == 0 ) {
		$Model->Parameters->setParameter( "screen_size", $post_data->screen_size );
	}
	
	// Save Elements
	foreach( $post_data->elements as $d ) {
		
		$tmp = array(
			"name_rus" => $d->name_rus,
			"name_eng" => $d->name_eng,
			"level" => $d->level,
			"label_x" => $d->label_x,
			"parent_id" => $d->parent_id,
			"label_y" => $d->label_y,
			"dot_x" => $d->dot_x,
			"dot_y" => $d->dot_y,
			"stroke_handle1_x" => $d->stroke_handle1_x,
			"stroke_handle1_y" => $d->stroke_handle1_y,
			"stroke_handle2_x" => $d->stroke_handle2_x,
			"stroke_handle2_y" => $d->stroke_handle2_y,			
			"color" => $d->color,
			'temp_parent_id' => $d->temp_parent_id,
			'temp_id' => $d->temp_id,
		);

		if( intval( $d->id ) != 0 ){
			$tmp[ 'id' ] = $d->id;
			$update_array[] = $tmp;
			$not_delete_array[] = intval( $d->id );
			
		} else {						
			$insert_array[] = $tmp;
		}
		
				
	}
	
	$Model->Elements->deleteElements( $not_delete_array );
	
	if( count( $update_array ) > 0 )
		$Model->Elements->updateElements( $update_array );
		
	// Insert new elements cascade	
	if( count ( $insert_array ) > 0 ){
					
		$level = 1;
		
		$completed = array();
		
		//start endless loop
		while( true ) {
			
			// break if there is no more elements
			if( count( $insert_array ) == 0 )
				break;
			
			// do throw element list
			foreach( $insert_array as $key => $el ){
				
				if ( $el[ 'level' ] == $level  ){
					
					// set parent id for level 1
					if( $el[ 'temp_parent_id' ] == 1 )
						$el[ 'parent_id' ] == 0;
					
					// get parent id from completed
					if( $el[ 'parent_id' ] == 0 )
						foreach( $completed as $comp ){
						
							if( $comp[ 'temp_id' ] == $el[ 'temp_parent_id'] ) {
								$el[ 'parent_id' ] = $comp[ 'id' ];
								break;
							}
							
						}
					
					//print_r( $el );	
					$real_id = $Model->Elements->insertElement( $el );
					
					// add element to completed
					$el[ 'id' ] = $real_id;
					$completed[] = $el;
					
					unset( $insert_array[ $key ] );
					
				}
			}
			
			$level ++;
		}
				
	}
	
	echo 1;		
	wp_die();
}

?>