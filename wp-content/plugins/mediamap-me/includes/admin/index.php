<?php

require_once(MEDIAMAP_DIR.'includes/admin/model.php');
require_once(MEDIAMAP_DIR.'includes/admin/ajax.php');
global $wpdb;

/**
 * Register a custom menu page.
 */
function mediamap_meAddMenu() {
    $page_title = "Настройка MediaMap.Me";
    $menu_title = "Карта элементов";
    $capability = "manage_options";
    $menu_slug = "mediamap-me";
    //$output_function = function(){ echo ( MEDIAMAP_DIR."includes/admin/mediamap.php" ); };
    $output_function = function(){
        include_once( MEDIAMAP_DIR."includes/admin/mediamap.php" );
    };
    $icon_url = "";
    $position = 2;
    
    add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $output_function, $icon_url, $position );
}

add_action( 'admin_menu', 'mediamap_meAddMenu' );

function mediamap_meAddMenu2() {
    $page_title = "YouTube видео";
    $menu_title = "YouTube видео";
    $capability = "manage_options";
    $menu_slug = "mediamap-me2";
    $output_function = function(){
        include_once( MEDIAMAP_DIR."includes/admin/youtube.php" );
    };
    $icon_url = "";
    $position = 3;
    
    add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $output_function, $icon_url, $position );
}

add_action( 'admin_menu', 'mediamap_meAddMenu2' );

function _wpse206466_can_view()
{
    // or any other admin level capability
    return current_user_can('manage_options');
}


add_action('load-index.php', 'wpse206466_load_index');
function wpse206466_load_index()
{
    if (!_wpse206466_can_view()) {
        header("Location: /");
        exit;
    }
}

add_action('admin_menu', 'wpse206466_remove_index');
function wpse206466_remove_index()
{
    if (!_wpse206466_can_view()) {
        remove_menu_page('index.php');
    }
}
