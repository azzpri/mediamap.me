<?php
    require_once ( MEDIAMAP_DIR.'includes/admin/model.php' );
    
    global $Model;
    $sucess = false;
    
    if ( $_SERVER['REQUEST_METHOD'] === 'POST' && isset( $_POST[ 'youtube_url' ] ) ) {
       $Model->Parameters->setParameter( "youtube_url", $_POST[ 'youtube_url' ] );
       $sucess = true;
    }
        
    $link = $Model->Parameters->getParameter( "youtube_url" );

    if( count( $link ) > 0 )
        $link = $link[ 0 ]->value;
    else
        $link = '';
        
?>

<link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo MEDIAMAP_URL; ?>assets/stylesheet.css" type="text/css">

<script type="text/javascript" src="<?php echo MEDIAMAP_URL; ?>assets/jquery.min.js"></script>        
                
<div class="row ">
    <div class="name-field">
        <?php if ( $sucess ): ?>
            <p class=".bg-success">Изменения успешно сохранены!</p>
        <?php endif; ?>
        <form method="post">
            <label for="link">Ссылка на видео</label>
            <input type="text" id="link" class="name-field" name="youtube_url" value="<?php echo $link; ?>" />
            <input type="submit" id="save" class="button" value="Сохранить"/>
        </form>
</div>



