<?php
    // get ScreenSize

    global $wpdb;
    
    $screen_size = $wpdb->get_results( 'select value from wp_mm_parameters where name = "screen_size"' );
    if( count( $screen_size ) > 0 )
        $screen_size = $screen_size[ 0 ]->value;
    else
        $screen_size = 1440;
?>
<link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo MEDIAMAP_URL; ?>assets/stylesheet.css" type="text/css">

<script type="text/javascript" src="<?php echo MEDIAMAP_URL; ?>assets/jquery.min.js"></script>        
<script type="text/javascript" src="<?php echo MEDIAMAP_URL; ?>assets/paper-full.min.js"></script>
<script src='//cdn.tinymce.com/4/tinymce.min.js'></script>

<script type="text/javascript">
    window.MM = window.MM || { };
    MM.base_screen_size = <?php echo $screen_size; ?>;
</script>                        


<div class="row controls">
    <div class="name-field">
        <input type="text" id="name_rus" class="name-field" name="name_rus" value="Русское название" readonly="readonly"/>
        <input type="text" id="name_eng" class="name-field" name="name_eng" value="Английское название" readonly="readonly"/>
    </div>
    <input type="button" id ="add" class="button" name="diagram_add_child" value="Добавить" disabled="disabled"/>
    <input type="button" id="remove" class="button" name="diagram_remove_child" value="Удалить" disabled="disabled"/>
    <input type="button" id="cards" class="button" value="Карточки" disabled="disabled"/>    
    <input type="button" id="cancel" class="button" value="Отменить"/>
    <input type="button" id="save" class="button" value="Сохранить"/>
    <input type="button" id="zoom-in" class="button" value="+"/>
    <input type="button" id="zoom-out" class="button" value="-"/>
    
</div>
<div id="hover" class="hover">
    <div class="hover-wrap">
        <div class="head">
            <h2>Управление карточами элемента</h2>            
            <input type="button" id="cards-cancel" class="button" value="Отменить"/>
            <input type="button" id="cards-save" class="button" value="Сохранить"/>
        </div>          
        <div class="content">
            <h3>Активные карточки</h3>
            <div class="buttons">                
                <input type="button" id="cards-add" class="button" value="Добавить"/>
                <input type="button" id="cards-edit" class="button" value="Редактировать" disabled="disabled"/>
                <input type="button" id="cards-delete" class="button" value="Удалить" disabled="disabled"/>
            </div>
            <div class="container">
                <div><input type="checkbox" card_id=1 /> <label>This is checkbox</label></div>
                <div><input type="checkbox" card_id=1 /> <label>This is checkbox</label></div>
                <div><input type="checkbox" card_id=1 /> <label>This is checkbox</label></div>
                <div><input type="checkbox" card_id=1 /> <label>This is checkbox</label></div>
                <div><input type="checkbox" card_id=1 /> <label>This is checkbox</label></div>
                <div><input type="checkbox" card_id=1 /> <label>This is checkbox</label></div>
            </div>
        </div>
    </div>    
</div>
<div id="hover2" class="hover">
    <div class="hover-wrap">
        <div class="head">
            <h2>Добавление карточки</h2>
            <input type="button" id="card-cancel" class="button" value="Отменить"/>
            <input type="button" id="card-save" class="button" value="Готово"/>
        </div>
        <div class="content">
            <p><b>Название</b></p>
            <input type="text" id="card-name-rus" class="name-field" name="card_name_rus" value="Русское название"/><br>
            <p><b>Название (английский)</b></p>
            <input type="text" id="card-name-eng" class="name-field" name="card_name_eng" value="Английское название"/>
            <br>
            <br>
            <p><b>Действие</b></p>
            <input type="text" id="card-action-rus" class="name-field" name="card_action_rus" value="Действие"/><br>
            <p><b>Действие (английский)</b></p>
            <input type="text" id="card-action-eng" class="name-field" name="card_action_eng" value="Действие (английский)"/>
            <br>
            <br>
            <p><b>Текст</b></p>
            <textarea id="card-description-rus" name="card_description_rus">
            </textarea>  
            <?php /*wp_editor( '', 'card-description-rus', array() );*/ ?>
            <p><b>Английский текст</b></p>
            <textarea id="card-description-eng" name="card_description_eng">
            </textarea>
            <!--<input type="textarea" id="card-description-eng" class="name-field" name="card_description_eng" value="Английский текст"/><br>-->
            <?php /*wp_editor( '', 'card-description-eng', array() );*/ ?>
        </div>
    </div>
</div>
<canvas id="myCanvas" resize="true">    
</canvas>

<script type="text/javascript" src="http://mediamap.me/wp-content/themes/mediamap/js/mediamap-common/Mediamap.Canvas.js"></script>
<script type="text/javascript" src="http://mediamap.me/wp-content/themes/mediamap/js/mediamap-common/Mediamap.common.js"></script>
<script type="text/javascript" src="http://mediamap.me/wp-content/themes/mediamap/js/mediamap-common/Mediamap.Hexagon.js"></script>
<script type="text/javascript" src="<?php echo MEDIAMAP_URL; ?>assets/Ajax.js" ></script> 
<script type="text/javascript" src="<?php echo MEDIAMAP_URL; ?>assets/Controls.js" ></script>
<script type="text/javascript" src="<?php echo MEDIAMAP_URL; ?>assets/Element.js" ></script>
<script type="text/javascript" src="<?php echo MEDIAMAP_URL; ?>assets/index.js" ></script>
