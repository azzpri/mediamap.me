<?php

global $Model;

class Model {
    
    public function db_get( $sql ){
        global $wpdb;
        return $wpdb->get_results( $sql );        
    }
		
}

class ModelElements extends Model {
	/*
	 *	Get Elements
	 */
	public function getElements() {
		return $this->db_get( "select * from wp_mm_elements;" );
	}
	/*
	 *	Update Elements
	 */
	public function updateElements( $array ) {
		global $wpdb;
					
		foreach( $array as $el_data ) {
															
			$wpdb->update( 
				'wp_mm_elements',
				array(
					"name_rus" => $el_data[ 'name_rus' ],
					"name_eng" => $el_data[ 'name_eng' ],
					"level" => $el_data[ 'level' ],
					"label_x" => $el_data[ 'label_x' ],
					"parent_id" => $el_data[ 'parent_id' ],
					"label_y" => $el_data[ 'label_y' ],
					"dot_x" => $el_data[ 'dot_x' ],
					"dot_y" => $el_data[ 'dot_y' ],
					"stroke_handle1_x" => $el_data[ 'stroke_handle1_x' ],
					"stroke_handle1_y" => $el_data[ 'stroke_handle1_y' ],
					"stroke_handle2_x" => $el_data[ 'stroke_handle2_x' ],
					"stroke_handle2_y" => $el_data[ 'stroke_handle2_y' ],									
					"color" => $el_data[ 'color' ]
				),
				array( 'id' => $el_data['id'] ), 
				array( 
					'%s',
					'%s',
					'%d',
					'%s',
					'%d',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',									
					'%s',
				)
			);				
		}

	}
	/*
	 *	Insert Elements
	 */
	public function insertElement( $el_data ) {
		
		global $wpdb;
								
		$wpdb->insert( 
			'wp_mm_elements',
			array(
				"name_rus" => $el_data[ 'name_rus' ],
				"name_eng" => $el_data[ 'name_eng' ],				
				"level" => $el_data[ 'level' ],
				"parent_id" => $el_data[ 'parent_id' ],				
				"label_x" => $el_data[ 'label_x' ],
				"label_y" => $el_data[ 'label_y' ],
				"dot_x" => $el_data[ 'dot_x' ],
				"dot_y" => $el_data[ 'dot_y' ],
				"stroke_handle1_x" => $el_data[ 'stroke_handle1_x' ],
				"stroke_handle1_y" => $el_data[ 'stroke_handle1_y' ],
				"stroke_handle2_x" => $el_data[ 'stroke_handle2_x' ],
				"stroke_handle2_y" => $el_data[ 'stroke_handle2_y' ],
				"color" => $el_data[ 'color' ]
			),					
			array( 
				'%s',
				'%s',				
				'%d',
				'%d',				
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',				
				'%s',
				'%s',
				'%s'
			)
		);
		
		return $wpdb->insert_id;
	}
	/*
	 *	Delete Elements
	 */
	public function deleteElements( $array ) {
		
		global $wpdb;
		
		if( count( $array) > 0 )
			$sql = 'delete from wp_mm_elements where id not in (' . implode( ', ', $array ) . ')';
		else
			$sql = 'delete from wp_mm_elements';
			
		$wpdb->query( 
			$wpdb->prepare(
				$sql
			)
		);			
	}
}

$Model->Elements = new ModelElements();

/*
 *	ScreenSize model
 */
class ModelParameters extends Model {
    /*
	 *	Get 
	 */
	public function getParameter ( $name ) {
		return $this->db_get( 'select * from wp_mm_parameters where name ="' . $name . '";' );
	}
	/*
	 *	Set
	 */
	public function setParameter ( $name, $value ) {
		global $wpdb;
		
		$db_entry = $this->getParameter( $name );
		
		if( count( $db_entry ) > 0 ){
			$wpdb->update( 
				'wp_mm_parameters',
				array( 'value' => $value ),
				array( 'id' => $db_entry[0]->id ), 
				array( '%s')
			);					
		} else {
			$wpdb->insert( 
				'wp_mm_parameters',
				array( 'name' => $name, 'value' => $value ),
				array( '%s', '%s' )
			);
		}
	}
}

$Model->Parameters = new ModelParameters();

/*
 *	UsersElements model
 */
class ModelUsersElements extends Model {
	/*
	 *	Get 
	 */
	public function getUserElements ( $user_id, $type ) {
		
		$sql = 'select * from wp_mm_users_elements where user_id =' . $user_id ;
		
		if( isset( $type ) )
			$sql .= " and type =" . $type;
				
		return $this->db_get( $sql );
	}
	/*
	 *	Remove
	 */
	public function removeUserElements ( $user_id, $array ) {
		global $wpdb;
					
		$wpdb->query( 
			$wpdb->prepare(
				'delete from wp_mm_users_elements where user_id = '. $user_id .' and element_id in (' . implode( ', ', $array ) . ')'
			)
		);	
	}
	/*
	 *	Set
	 */
	public function addUserElements ( $user_id, $values ) {
		global $wpdb;
	
		$val_row = "";
	
		foreach( $values as $v ) {
			if( strlen( $val_row ) > 0 ) $val_row .= ",";			
			$val_row .= "(" . $v[0] . "," . $v[1] . "," . $v[2] . ")";
		}
		
		$a = $wpdb->prepare( "INSERT INTO wp_mm_users_elements (user_id, element_id, type) VALUES " . $val_row );
		
		print_r( $a );
		
		$wpdb->query( $a );

	}
	
}
$Model->UsersElements = new ModelUsersElements();

/*
 *	Cards model
 */
class ModelCards extends Model {
	/*
	 *	Get 
	 */
	public function getCardsForElement ( $element_id ) {
		return $this->db_get( "select c.*, coalesce( NULLIF(ec.card_id,'') , '0' ) as enabled from wp_mm_cards c left outer join (select card_id from wp_mm_elements_cards where element_id = ". $element_id .") ec on ec.card_id = c.id" );
	}
	public function getCardsForElementOnly ( $element_id ) {
		return $this->db_get( "select c.* from wp_mm_cards c, wp_mm_elements_cards ec where element_id = ". $element_id ." and ec.card_id = c.id" );
	}
	public function getUserCards ( $user_id, $type ) {		
		return $this->db_get( "select c.*, ec.element_id from wp_mm_cards c, wp_mm_elements_cards ec where c.id = ec.card_id and ec.element_id in ( select element_id from wp_mm_users_elements where user_id = ". $user_id ." and type = ". $type ." )" );
	}
	
	/*
	 *	Remove card
	 */
	public function removeCard ( $card_id ) {
		global $wpdb;
					
		$wpdb->query( 
			$wpdb->prepare(
				'delete from wp_mm_cards where id = '. $card_id
			)
		);	
	}
	/*
	 *	Add card
	 */
	public function addCard ( $data ) {
		global $wpdb;
					
		$wpdb->insert( 
				'wp_mm_cards',
				array(
					  'name_rus' => $data->name_rus,
					  'name_eng' => $data->name_eng,
					  'action_rus' => $data->action_rus,
					  'action_eng' => $data->action_eng,
					  'text_rus' => $data->text_rus,
					  'text_eng' => $data->text_eng
					),
				array( '%s', '%s','%s', '%s','%s', '%s' )
			);				
	}
	/*
	 *	Add Element Card
	 */
	public function addElementCard ( $element_id, $card_id ) {
		global $wpdb;
		
		$wpdb->query( $wpdb->prepare(
			"insert ignore into wp_mm_elements_cards set element_id = ". $element_id .", card_id =" . $card_id
		));				
	}
	/*
	 *	Remove Element Card
	 */
	public function removeElementCard ( $element_id, $card_id ) {
		global $wpdb;
		
		$wpdb->query( $wpdb->prepare(
			"delete from wp_mm_elements_cards where element_id = ". $element_id ." and card_id =" . $card_id
		));				
	}
	/*
	 *	Update card
	 */
	public function updateCard ( $data ) {
		global $wpdb;
					
		$wpdb->update( 
				'wp_mm_cards',
				array(
					  'name_rus' => $data->name_rus,
					  'name_eng' => $data->name_eng,
					  'action_rus' => $data->action_rus,
					  'action_eng' => $data->action_eng,
					  'text_rus' => $data->text_rus,
					  'text_eng' => $data->text_eng
					),
				array( 'id' => $data->id ), 
				array( '%s', '%s','%s', '%s','%s', '%s' )
			);				
	}	
}
$Model->Cards = new ModelCards();
?>