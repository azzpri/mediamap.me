window.MM = window.MM || { };

/**
 *		Elements Class
 */
MM.Elements = new function( ){
	
	this._last_id = 0;
	this.storage = {};
	this.colors = [ "#4fd7cd", "#0da5e4", "#f86264", "#df8cde", "#7d6fc6", "#aee355" ];
	
	this.newID = function( ) {
		return ++this._last_id;		
	}
	
	this.save = function( el ){
		if ( ! el.id ) {
            el.id = this.newID();
        }		
		this.storage[ el.id ] = el;
	}
	
	this.remove = function( el ){
		delete this.storage[ el.id ];
	}
	
	this.getElementByID = function ( id ){
		
		for ( var p in this.storage ) {
			if ( this.storage.hasOwnProperty( p ) ) {				
				if ( this.storage[ p ].db_id == id ) {					
					return this.storage[ p ];
				}
			}
		}
	}
	
	/*
	 *	Blur all 1 level elements, except el
	 */
	this.blurFirstElements = function ( args ) {
		
		var opacity = 0.5;
		var el = false;
		
		if ( args.hasOwnProperty( 'opacity' ) )
            opacity = args.opacity;
        
		if ( args.hasOwnProperty( 'element' ) )
            el = args.element;
				
		for ( var k in MM.Elements.storage ){
			var e = MM.Elements.storage[ k ];
			if ( e.level == 1 ) {				
				if ( ( el && e.id != el.id ) || ! el ) {					
					e.Label.opacity = opacity;
				}
			}
		}
		
		if ( el ) {
			el.Label.opacity = 1;
		}
	}
	
	this.getColor = function () {
		
		var new_color = this.colors[ 0 ];
		this.colors.shift( new_color );
		this.colors.push( new_color );
		
		return new_color;
		
	}
};

/**
 *		Element Class
 */
MM.Element = function () {
	
	this.id = false;
	this.db_id = false;
	this.Dot = false;
	this.Stroke = false;
	this.Label = false;
	this.level = false;
	this.branch_id = false;
	
	this.parent = false;
	this.children = [];
	this.name_eng = false;
	this.name_rus = false;

	this._default_color = "white";
	
	// Element initialization
	this.init = function ( data ){
				
		if ( typeof( data ) === 'undefined' ) {
            data = {};
        }
		
		if ( data.hasOwnProperty( 'db_id' ) ) {
			this.db_id = data.db_id;
		} else {
			this.db_id = 0;
		}
		
		// set default position for root
		if ( ! data.hasOwnProperty( 'dot_position' ) ) {            
			data.dot_position = { x : $('#myCanvas').width() / 2, y : $('#myCanvas').height() / 2 };	
		}						
		
		//add parent
		if ( data.hasOwnProperty( 'parent' ) ) {		
            this.parent = data.parent;
			this.parent.children.push( this );
		}
		
		// set level
		if ( ! this.parent ) {
            this.level = 0;
        } else {
			this.level = this.parent.level + 1;
			
			if ( MM.Elements.top_level < this.level ) {
                MM.Elements.top_level = this.level;
            }
		}
		
		// set default color
		if ( data.hasOwnProperty( 'color' ) ) {
			this._default_color = data.color;
		} else {
			if ( this.level == 1 ) {
                this._default_color = MM.Elements.getColor();
            }
			if (this.level > 1 ) {
				this._default_color = this.parent._default_color;
			}
		}
		
		// set style		
		this.style = MM.ElementStyle.style( this.level );
		
		// draw Dot
		this._drawDot( data.dot_position );
		
		// draw Stroke
		if ( data.hasOwnProperty( 'stroke_handle1_position' ) ) {
			if ( data.hasOwnProperty( 'hexagon_point' ) ) {
				this._drawStroke( data.stroke_handle1_position, data.stroke_handle1_position, data.hexagon_point );
			} else {
				this._drawStroke( data.stroke_handle1_position, data.stroke_handle1_position, false );
			}
		} else {
			this._drawStroke( );
		}
		
		
		// draw Label		
		if ( data.hasOwnProperty( 'title' ) ){
			
			if ( data.hasOwnProperty( 'label_position' ) ) {
				this._drawLabel( data.title, data.label_position );                
            }else{
				this._drawLabel( data.title, { x: data.dot_position.x + 5, y: data.dot_position.y + 5 } );				
			}
						
		}
				
		// save object changes
		this.saveToElements();
		
		//set branch id
		if ( this.level > 0 ) {
			if ( this.level == 1 ) {
				this.branch_id = this.id;
			} else {
				this.branch_id = this.parent.branch_id;
			}
		}					
	}
	
	// save element
	this.saveToElements = function () {
        MM.Elements.save( this );
	}
	
	// remove element
	this.deleteFromElements = function () {
		MM.Elements.remove( this );		
	}
	
	// Set Base Colors
	this.setBaseColors = function () {
		this.Dot.strokeColor = this._default_color;
		this.Dot.fillColor = this._default_color;
		
		if ( this.Label ) {
            this.Label.fillColor = this._default_color;
        }
		
		if ( this.Stroke ) {
            this.Stroke.strokeColor = this._default_color;
        }
	}
	
	// Draw Dot
	this._drawDot = function ( position ) {
							
		var _this = this;
		
		if (this.level == 0) {
            var radius = 10;
        }else {			
            var radius = 5;			
		}
				                
		this.Dot = new Path.Circle( {
			center: new Point( parseInt( position.x ), parseInt( position.y ) ),
			radius: radius,
			strokeColor: this._default_color,
			fillColor: this._default_color,
		}).attach('click', function () {
			_this._onElementSelect( _this );
		});
		
		this.Dot.data.position =  {x: parseInt( position.x ), y: parseInt( position.y )};
		this.Dot.opacity = 0.5;
		                    
		this.Dot.onMouseDrag = function( event ){
									
			if ( _this.level == 0 && MM.Controls.selected_element.level == 1 ) {
				MM.Controls.selected_element.Stroke.firstSegment.point.x += event.delta.x;
				MM.Controls.selected_element.Stroke.firstSegment.point.y += event.delta.y;
			}
				
			if ( _this.parent ) {
				_this.Dot.position.x += event.delta.x;
				_this.Dot.position.y += event.delta.y;	
					
				_this.Stroke.lastSegment.point.x += event.delta.x;
				_this.Stroke.lastSegment.point.y += event.delta.y;
			
				for( var i = 0; i < _this.children.length; i++ ){
					_this.children[ i ].Stroke.firstSegment.point.x += event.delta.x;
					_this.children[ i ].Stroke.firstSegment.point.y += event.delta.y;
				}								
			}
			
			_this.saveToElements();
		}
		
		
	};
	
	// Draw Stroke
	this._drawStroke = function ( h1, h2, hexagon_point ) {
		
		if ( ! this.parent )
            return;
        				
		var start = new Point( this.parent.Dot.position.x, this.parent.Dot.position.y );
						
		if ( this.level == 1 ) {
			start = MM.Hexagon.polygon.segments[ hexagon_point - 1 ].point;   
        }
		
		var end = new Point( this.Dot.position.x, this.Dot.position.y );
				
		var handleX = ( start.x + end.x ) / 2 - start.x + Math.cos( start.angle ) *
				Math.sqrt( Math.pow( end.x - start.x, 2 ) + Math.pow(  end.y - start.y, 2 ) )/2;
		var handleY = ( start.y + end.y ) / 2 - start.y + Math.sin( start.angle ) *
				Math.sqrt( Math.pow( end.x - start.x, 2 ) + Math.pow( end.y - start.y, 2 ) )/2;		
		
		var segments = [
			new Segment( {
				point: start,
			    handleOut: [ handleX, handleY ]
			}),
			new Segment( {
				point: end,
			    handleIn: [ 0, 0 ]
			}),            
            //end                			
		];
		
		var _this = this;
		var path = new Path( segments ).attach( 'click', function () {
			_this._onElementSelect( _this );
		});
		
		if ( typeof( h1 ) !== 'undefined' ) {
			path.firstCurve.handle1.x = parseInt( h1.x );
			path.firstCurve.handle1.y = parseInt( h1.y );
			path.lastCurve.handle2.x = parseInt( h2.x );
			path.lastCurve.handle2.y = parseInt( h2.y );
        }
		
		path.strokeColor = _this._default_color;
        path.strokeWidth = 3;
		path.opacity = 0.3;
		
        path.onMouseDrag = function( event ){            
			path.firstCurve.handle1.x += event.delta.x;
			path.firstCurve.handle1.y += event.delta.y;
			
			path.firstCurve.handle2.x += event.delta.x;
			path.firstCurve.handle2.y += event.delta.y;
			
			_this.saveToElements();
        }
		
		this.Stroke = path;
        
	};
	
	// Draw Label
	this._drawLabel = function ( title, label_position ) {
		
		var _this = this;
		
		_this.name_rus = title.name_rus;
		_this.name_eng = title.name_eng;
		
		this.Label = new PointText( {
			point 		: new Point( label_position.x, label_position.y ),
			content		: title.name_rus,
			fillColor	: this._default_color,
			fontFamily	: 'PT Sans',
			fontSize	: _this.style.labelFontSize,
		}).attach( 'click', function () {
			_this._onElementSelect( _this );
		});
				
		this.Label.opacity = _this.style.labelOpacity;

		if ( this.db_id != 0 ) {
			this.Label.bounds.x = label_position.x;
			this.Label.bounds.y = label_position.y;
        }
				
		this.Label.onMouseDrag = function( event ){
			_this._onElementDrag( event, _this );	
		}
		
		_this.onChangeLabelEng( title.name_eng );
	};
	
	// on change English text
	this.onChangeLabelEng = function ( name ) {
		
		if ( typeof ( this.LabelShadow )  !== 'undefined' ) {
			this.LabelShadow.remove();
		}
			
		if ( typeof ( name )  !== 'undefined' ) {   
			
			this.name_eng = name;
				
			// Draw English shadow
			var eng_point_x = this.Label.bounds.x;
			var eng_width = this.Label.bounds.width * this.name_eng.length / this.name_rus.length;
			
			if ( this.Dot.data.position.x > this.Label.bounds.x + this.Label.bounds.width/2 ) {
				eng_point_x = this.Label.bounds.x + this.Label.bounds.width - eng_width;				
			}
			
			this.LabelShadow = new Shape.Rectangle( {
				point		: new Point( eng_point_x, this.Label.bounds.y),
				size		: [ eng_width, this.Label.bounds.height ],
				fillColor 	: this._default_color,
				opacity		: 0.15
			} );
			
			this.LabelShadow.bounds.x = eng_point_x;
			this.LabelShadow.bounds.y = this.Label.bounds.y;
		
		}
	}
	
	// on select Element
	this._onElementSelect = function ( _this ) {
		                
		// check if previous Dot selected
		if ( MM.Controls.selected_element ) {
            MM.Controls.selected_element.setBaseColors();
        }
		
		// enable controls for non root element
		if ( ! _this.parent ) {
			MM.Controls.enableControls( 'add' );
        } else {
			MM.Controls.enableControls( 'edit' );
			MM.Controls.setText( _this.Label.content, _this.name_eng );
			
			_this.Stroke.strokeColor = "blue";                                    
			_this.Label.fillColor = "blue";
			
			if ( _this.children.length == 0 ) {
                MM.Controls.enableCards();
            }
            
		}
						
		MM.Controls.selected_element = _this;
		MM.Controls.clickOnElement = true;
				
		_this.Dot.strokeColor = "blue";
		_this.Dot.fillColor = "blue";
				
	};
	
	// on drag Element
	this._onElementDrag = function ( event, _this ) {
        
        if ( ! _this.parent ) {
            return;
        }
        
        _this.Label.bounds.x += event.delta.x;
		_this.Label.bounds.y += event.delta.y;		
        
        _this.Dot.position.x += event.delta.x;
		_this.Dot.position.y += event.delta.y;
                
        _this.Stroke.lastSegment.point.x += event.delta.x;
		_this.Stroke.lastSegment.point.y += event.delta.y;
		
		for( var i = 0; i < _this.children.length; i++ ){
			_this.children[ i ].Stroke.firstSegment.point.x += event.delta.x;
			_this.children[ i ].Stroke.firstSegment.point.y += event.delta.y;
		}
		
		_this.saveToElements();
	};
	
	// Remove element and his child
	this.remove = function () {
		
		if ( ! this.parent )
            return;
        
		for( var i = 0; i < this.children.length; i++ ) {
			var ans = this.children[ i ].remove();
		}
		
		this.Dot.remove();
		this.Stroke.remove();
		this.Label.remove();
		
		this.deleteFromElements();
		
		return 1;
	}
}