/**
 *		Element Class
 */
window.MM = window.MM || { };    

/*
 *	Ajax
 */
MM.Ajax = MM.Ajax || { };
	
// Get Elements
MM.Ajax.getElements = function ( callback ) {
	
	var ans = [];
	
	$.ajax( {
		type: "GET",
		url: "admin-ajax.php",
		data: {
			action : 'getElements'
		},
		success: function ( response ) {							
			callback( response );								
		}
	} );				
}

// Save Elements
MM.Ajax.saveElements = function ( elements_data ) {
	
	var data = {
		"elements" : elements_data,
		"screen_size" : paper.view.size.width
	};
			
	$.ajax( {
		type: "POST",
		url: "admin-ajax.php?action=updateElements" ,
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		async: false,
		data : JSON.stringify( data ),			
		success: function ( response ) {
			if ( response == 1 ) {
				alert( "Изменения успешно сохранены!" );
				return;
			}
			
			alert( "При сохранении в базу произошла ошибка!" );
		},
		error: function ( response ) {
			alert( "При сохранении в базу произошла ошибка!" );
		}
		
	} );
	
}
		
// Update Cards
MM.Ajax.updateCards = function ( data, element_id, callback ) {
				
	$.ajax( {
		type: "POST",
		url: "admin-ajax.php?action=cards" ,
		data: JSON.stringify( {
			action : 'updateCards',
			data : data,
			element_id : element_id
		}),
		success: function ( response ) {
			if ( response == 1 ) {
				
				callback();
				return;
			}
			
			alert( "При сохранении в базу произошла ошибка!" );
		},
		error: function ( response ) {
			alert( "При сохранении в базу произошла ошибка!" );
		}
		
	} );		
}

// Delete Card
MM.Ajax.deleteCard = function ( data ) {
	$.ajax( {
		type: "POST",
		url: "admin-ajax.php?action=cards" ,
		data: JSON.stringify( {
			action : 'deleteCard',
			card_id : data
		}),
		success: function ( response ) {
			if ( response == 1 ) {										
				return;
			}
			
			alert( "При сохранении в базу произошла ошибка!" );
		},
		error: function ( response ) {
			alert( "При сохранении в базу произошла ошибка!" );
		}
		
	} );			
}


