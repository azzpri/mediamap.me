/**
 *		Controls Class
 */

window.MM = window.MM || { };

/*
 *  CardControls
 */
MM.CardControls = new function () {
	
	this.selected_element_index = -1;
	
	this._elements = {
		'hover' 			: $("#hover2"),
		'save' 				: $("#card-save"),
		'cancel' 			: $("#card-cancel"),
		'name_rus'			: $("#card-name-rus"),
		'name_eng'			: $("#card-name-eng"),
		'action_rus'		: $("#card-action-rus"),
		'action_eng'		: $("#card-action-eng"),
		'description_rus'	: $("#card-description-rus"),
		'description_eng'	: $("#card-description-eng"),
		'text'				: $("#hover2 .head h2")
	}
	
	// Init
	this.init = function () {
		
		var _this = this;
		
		// Click Cancel
		this._elements.cancel.on( 'click', function ( ) {
			if( confirm( "Вы уверены, что хотите отменить изменения?" ) ){
				_this._elements.hover.hide();			
			}
		});
		
		// Click Save
		this._elements.save.on( 'click', function ( ) {
			
			var data = {
					'name_rus'	 	: _this._elements.name_rus.val(),
					'name_eng'	 	: _this._elements.name_eng.val(),					
					'text_rus'	 	: tinyMCE.editors[0].getContent(),
					'text_eng'	 	: tinyMCE.editors[1].getContent(),
					'action_rus' 	: _this._elements.action_rus.val(),
					'action_eng' 	: _this._elements.action_eng.val(),
				};
			
			
			if ( _this.selected_element_index < 0 ) {
				MM.CardListControls.addElement( data );
			} else {
				MM.CardListControls.editElement( _this.selected_element_index, data );
			}
			
			_this._elements.hover.hide();			
		});				
	}
	
	// Show
	this.show = function () {
				
		if ( this.selected_element_index > -1 ) {
            this._elements.text.html( "Редактирование карточки");
			this._elements.name_rus.val( MM.CardListControls.storage[ this.selected_element_index ].name_rus );
			this._elements.name_eng.val( MM.CardListControls.storage[ this.selected_element_index ].name_eng );
			this._elements.action_rus.val( MM.CardListControls.storage[ this.selected_element_index ].action_rus );
			this._elements.action_eng.val( MM.CardListControls.storage[ this.selected_element_index ].action_eng );
			tinyMCE.editors[0].setContent( MM.CardListControls.storage[ this.selected_element_index ].text_rus );
			tinyMCE.editors[1].setContent( MM.CardListControls.storage[ this.selected_element_index ].text_eng );			
        } else {
			this._elements.text.html( "Создание карточки" );
			this._elements.name_rus.val( '' );
			this._elements.name_eng.val( '' );
			tinyMCE.editors[0].setContent( '' );
			tinyMCE.editors[1].setContent( '' );
			this._elements.action_rus.val( '' );
			this._elements.action_eng.val( '' );
		}
		this._elements.hover.show();
	}
}

/*
 *
 */
MM.CardListControls = new function () {
	
	this._elements = {
		'hover' 					: $("#hover"),
		'container' 				: $(".container"),		
		'add' 						: $("#cards-add"),
		'edit' 						: $("#cards-edit"),
		'delete' 					: $("#cards-delete"),
		'element_name'				: $("#hover .head h2"),
		'cancel'					: $("#hover .head #cards-cancel"),
		'save'						: $("#hover .head #cards-save")
	}
	
	this._selector = {
		'checkbox' 					: ".container input",
		'checkbox_label' 			: ".container label",
		'selected_label' 			: ".selected-label",
	}
	
	this.storage = [];
	
	// Init
	this.init = function () {
		
		var _this = this;
		
		// Click checkbox name
		_this._elements.container.delegate( 'label' , 'click', function () {
			
			// change colors settings
			$( _this._selector.checkbox_label ).removeClass( 'selected-label' );
			$( this ).addClass( 'selected-label' );
			
			// activate buttons
			_this._elements.edit.attr( "disabled", null );
			_this._elements.delete.attr( "disabled", null );
			
		});
		
		// Click Add
		this._elements.add.on( 'click', function () {
			
			MM.CardControls.selected_element_index = -1;
			MM.CardControls.show();			
			
			$( _this._selector.checkbox_label ).removeClass( 'selected-label' );
			
			_this._elements.edit.attr( "disabled", "disabled" );
			_this._elements.delete.attr( "disabled", "disabled" );
			
		});
		
		// Click Edit
		this._elements.edit.on( 'click', function () {
			
			MM.CardControls.selected_element_index = $( _this._selector.selected_label ).parent().index();
			
			MM.CardControls.show();
			
			$( _this._selector.checkbox_label ).removeClass( 'selected-label' );
			
			_this._elements.edit.attr( "disabled", "disabled" );
			_this._elements.delete.attr( "disabled", "disabled" );
			
		});
		
		// Click Delete
		this._elements.delete.on( 'click', function () {
			
			if( confirm( "Вы уверены, что хотите удалить карточку?") ) {
											
				_this._elements.edit.attr( "disabled", "disabled" );			
				_this._elements.delete.attr( "disabled", "disabled" );
				
				var index = $( _this._selector.selected_label ).parent().index();
								
				if ( _this.storage[ index ].id > 0 ) {
					MM.Ajax.deleteCard( _this.storage[ index ].id );    
					_this.storage.splice( index, 1 );								
                }
				
				var el = $( _this._selector.selected_label ).parent();
				el.remove();
			}

		});
				
		// Click Cancel
		this._elements.cancel.on( 'click', function ( ) {
			
			if( confirm( "Вы увeрены, что хотите отменить изменения?" ) ){
				_this.toggleVisibility();			
			}
		});
			
		// Click Save
		this._elements.save.on( 'click', function ( ) {
						
			$( _this._selector.checkbox ).each( function () {
								
				var index = $( this ).parent().index();
				
				if ( _this.storage[ index ].id == 0  ) {
                    _this.storage[ index ].action = "add";
                }
				
				if ( $( this ).is(":checked") && _this.storage[ index ].enabled == 0  ) {
                    _this.storage[ index ].check = "check";
                }
				
				if ( ! $( this ).is(":checked") && _this.storage[ index ].enabled > 0  ) {
                    _this.storage[ index ].check = "remove_check";
                }

			} );
			
			MM.Ajax.updateCards( _this.storage, MM.Controls.selected_element.db_id,  _this.toggleVisibility );
						
		});
	}
	
	// Set element name
	this.setElementName = function ( name ) {
		this._elements.element_name.html( "Управление карточками элемента <span>" + name + "</span>" );
	}
	
	// ToggleVisibility
	this.toggleVisibility = function( ) {
		
		if ( MM.CardListControls._elements.hover.is(":visible") ) {
								
			MM.CardListControls._elements.edit.attr( "disabled", "disabled" );
			MM.CardListControls._elements.delete.attr( "disabled", "disabled" );
			
			MM.CardListControls._elements.hover.hide();
			
		} else {
			
			MM.CardListControls._elements.hover.show();
			
		}
	}
	
	// editElement
	this.editElement =  function ( index, data ) {
		var name 	= "без названия",
			enabled = "";
		
		if ( data.name_eng.length > 0 ) {
            name = data.name_eng;			
        }
		
		if ( data.name_rus.length > 0 ) {
            name = data.name_rus;			
        }
		
		this._elements.container.find("div:nth-child("+ index + 1 +") label").html( name );	
				
		this.storage[ index ].name_rus = data.name_rus;
		this.storage[ index ].name_eng = data.name_eng;
		this.storage[ index ].action_rus = data.action_rus;
		this.storage[ index ].action_eng = data.action_eng;
		this.storage[ index ].text_rus = data.text_rus;
		this.storage[ index ].text_eng = data.text_eng;
		this.storage[ index ].action = 'edit';
	}
		
	// Add Element
	this.addElement = function ( data ) {
		
		var name 	= "без названия",
			enabled = "";
		
		if ( data.name_eng.length > 0 ) {
            name = data.name_eng;			
        }
		
		if ( data.name_rus.length > 0 ) {
            name = data.name_rus;			
        }
		
		if ( data.enabled > 0 ) {
			enabled = "checked";
        }
		
		var el = $('<div><input type="checkbox" '+ enabled +'/ > <label>' + name + '</label> </div>').appendTo( this._elements.container );
		
		if ( typeof( data.id ) === 'undefined') {
            data.id = 0;
        }
		
		this.storage[ el.index() ] = data;
	}
	
	// remove all Elements
	this.removeElements = function () {
		$( this._elements.container ).html("");
	}
}
/*
 *
 */
MM.Controls = new function () {
	
	this.selected_element = false;
	this.clickOnElement = false;	
	
	this._elements = {
		'names' 	: $(".controls .name-field"),
		'name_rus' 	: $(".controls #name_rus"),
		'name_eng' 	: $(".controls #name_eng"),
		'remove' 	: $(".controls #remove"),
		'add' 		: $(".controls #add"),
		'save' 		: $(".controls #save"),
		'cancel' 	: $(".controls #cancel"),
		'cards' 	: $(".controls #cards"),
        'zoom_in' 	: $(".controls #zoom-in"),
        'zoom_out' 	: $(".controls #zoom-out"),
	}
    this.enableControls = function ( type ) {
		
		if ( type == 'edit' ) {
			this._elements.names.attr( "readonly", null );
			this._elements.remove.attr( "disabled", null );
        }
		
		this._elements.add.attr( "disabled", null );
		
	}
	
	this.enableCards = function () {
		this._elements.cards.attr( "disabled", null );
	}
	
	this.disableControls = function () {	
		this._elements.names.attr( "readonly", "readonly"  );
		this._elements.name_rus.val( "Русское название" );
		this._elements.name_eng.val( "Английское название" );
		this._elements.add.attr( "disabled", "disabled" );
		this._elements.cards.attr( "disabled", "disabled" );
		this._elements.remove.attr( "disabled", "disabled" );
	}
	
	this.setText = function ( rus, eng ) {
		if ( rus != 'Новый элемент' )
			this._elements.name_rus.val( rus );
		if ( typeof( eng ) !== 'undefined' ) 
            this._elements.name_eng.val( eng );			
	}
	
	this.init = function () {
		            
		var _this = this;
		
		MM.CardControls.init();
		MM.CardListControls.init();	
		
		// on click Add button
		this._elements.add.on( 'click', function (  ) {			
			var el = new MM.Element();
			            
			el.init( {
				'dot_position' : { x: _this.selected_element.Dot.position.x + 80, y : _this.selected_element.Dot.position.y - 30 },
				'parent' : _this.selected_element,
				'title' : { name_rus : "Новый элемент", name_eng : "New element" }
			} );						
		});
		
		// on change Name
		this._elements.names.on( 'keyup', function( event ){
			
			if( event.keyCode == 13 ) {
				
				_this._elements.names.blur();
				
				if ( $( this ).attr("id") == "name_rus"){
					_this.selected_element.Label.content = _this._elements.name_rus.val();
					_this._elements.name_eng.focus();				
				} else {					
					_this.selected_element.onChangeLabelEng( _this._elements.name_eng.val() );
				}
								
				_this.selected_element.saveToElements();
				
			}
			
			if( event.keyCode == 27 ) {
				if ( $( this ).attr("id") == "name_rus")
					_this._elements.name_rus.val( _this.selected_element.Label.content );				
				_this._elements.names.blur();
			}
		});

		// on click Cancel
		this._elements.cancel.on( 'click', function( ) {
			if ( confirm( "Вы уверены, что хотите отменить свои изменения?" ) ) {
                location.reload();
            }
		} );
		
		// on click Save
		this._elements.save.on( 'click', function( ) {
			
            // return canvas to the initial state
            paper.view.center = MM.centerpoint;
            paper.view.zoom = 1;            
            
			var data = [];
			var i = 0;
			
			if ( MM.Elements.storage.length > 1 )
				data = [];
				
			for ( var p in MM.Elements.storage ) {
				if ( MM.Elements.storage.hasOwnProperty( p ) ) {
					
					var el = MM.Elements.storage[ p ];										
										
					if ( ! el.parent )
                        continue;					
					
					var obj = {
						"id" : el.db_id,
						"name_rus" : el.Label.content,
						"name_eng" : el.name_eng,						
						"level" : el.level,
						"label_x" : MM.relativeWidth( el.Label.bounds.x ),
						"label_y" : MM.relativeHeight( el.Label.bounds.y ),
						"dot_x" : MM.relativeWidth( el.Dot.position.x ),
						"dot_y" : MM.relativeHeight(el.Dot.position.y ),
						"stroke_handle1_x" : MM.relativeWidth( el.Stroke.firstCurve.handle1.x ),
						"stroke_handle1_y" : MM.relativeHeight(el.Stroke.firstCurve.handle1.y ),
						"stroke_handle2_x" : MM.relativeWidth( el.Stroke.lastCurve.handle2.x ),
						"stroke_handle2_y" : MM.relativeHeight( el.Stroke.lastCurve.handle2.y ),						
						"color": el._default_color,
						"temp_id" : el.id
					};
															
					if ( el.parent ) {
                        obj['parent_id'] = el.parent.db_id;
						obj['temp_parent_id'] = el.parent.id;
                    }
										
					data.push( obj );
				}
			}

			// send changes to server
			MM.Ajax.saveElements( data );
            
		} );
		
        //delete element        
        this._elements.remove.on( 'click', function (  ) {			
			
			if( ! _this.selected_element )
                return;
            
            _this.selected_element.remove();
            _this.disableControls();
			
		});
		
		// on click cards
		this._elements.cards.on( 'click', function () {
			MM.Ajax.getCards( MM.Controls.selected_element.db_id, function ( data ) {
				
				MM.CardListControls.removeElements();
								
				for( var i in data ) {
					MM.CardListControls.addElement( data[ i ] );
				}
				
				MM.CardListControls.setElementName( MM.Controls.selected_element.name_rus );
				MM.CardListControls.toggleVisibility();
			} );
		});
		
        // Zoom In        
        this._elements.zoom_in.on( 'click', function () {
            if ( paper.view.zoom + 0.3 < 2 ) {
                paper.view.zoom += 0.3;             
            }            
        } );
        
        // Zoom Out        
        this._elements.zoom_out.on( 'click', function () {
            if ( paper.view.zoom - 0.3 > 0 ) {
                paper.view.zoom -= 0.3;             
            }
        } );
	}
};
