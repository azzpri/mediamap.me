paper.install(window);

$( document ).ready( function (){
	
	paper.setup('myCanvas');
	
	// initialize libraries	
	MM.Controls.init();	
		
	// call default initial function
	MM.init( function () {
		
		//add background
		MM.background.source = 'http://mediamap.me/wp-content/themes/mediamap/images/background.jpg';		
		
		MM.Hexagon.drawPolygon( MM.centerpoint, 6, 100 );	
		MM.Hexagon.onClick = function () {};
		
		// initialize Elements
		var element = new MM.Element();
		element.init();
		
		MM.Ajax.getElements( function( data ){
					
			var dt = jQuery.parseJSON( data );
							
			for ( var i = 0; i < dt.length; i++ ) {
			
				var el = new MM.Element();			
				
				el.init( {
					'db_id' 					: dt[ i ].id,
					'dot_position' 				: {
						x: MM.absoluteWidth( dt[ i ].dot_x ),
						y: MM.absoluteHeight( dt[ i ].dot_y )
					},
					'label_position' 			: {
						x: MM.absoluteWidth( dt[ i ].label_x ),
						y: MM.absoluteHeight( dt[ i ].label_y )
					},					
					"stroke_handle1_position" 	: {
						x: MM.absoluteWidth( dt[ i ].stroke_handle1_x ),
						y: MM.absoluteHeight( dt[ i ].stroke_handle1_y )
					},
					"stroke_handle2_position" 	: {
						x: MM.absoluteWidth( dt[ i ].stroke_handle2_x ),
						y: MM.absoluteHeight( dt[ i ].stroke_handle2_y )
					},
					"hexagon_point" 			: dt[ i ].hexagon_point,
					'parent' 					: MM.Elements.getElementByID( dt[ i ].parent_id ),
					'title' 					: { name_rus : dt[ i ].name_rus, name_eng : dt[ i ].name_eng },
					'color' : dt[ i ].color
				} );			
			}
			
			paper.view.update();
		});		
		
		// add handlers		
		$("canvas").on( 'click', function(){		
				
			if ( MM.Controls.clickOnElement ){
				MM.Controls.clickOnElement = false;
				return;
			}        					
			
			if ( MM.Controls.selected_element ) {			
				MM.Controls.selected_element.setBaseColors();
				MM.Controls.disableControls();
				MM.Controls.selected_element = false;
			}
		
		});
		
		// drag canvas
		MM.background.on( 'mousedrag', function ( event ) {
			paper.view.center = [
				paper.view.center.x - event.delta.x*0.7,
				paper.view.center.y - event.delta.y*0.7
			];		
		} );
		
		tinymce.init({
		    selector: '#card-description-rus, #card-description-eng',
			height: 500,
			theme: 'modern',
			plugins: [
			  'advlist autolink lists link image charmap print preview hr anchor pagebreak',
			  'searchreplace wordcount visualblocks visualchars code fullscreen',
			  'insertdatetime media nonbreaking save table contextmenu directionality',
			  'emoticons template paste textcolor colorpicker textpattern imagetools'
			],
			toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
			toolbar2: 'print preview media | forecolor backcolor emoticons',
			image_advtab: true,			
			content_css: [
			  '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
			  '//www.tinymce.com/css/codepen.min.css'
			]
		});
	} );
});
