<?php
    // Check Cookie
    $was_here = true;
    $english = false;
    
    ob_start();
    setcookie("was_here", "Yes, I'm really was", time() + 3600 * 24 * 14 );
    
    if( isset( $_GET[ "en" ] ) || isset( $_COOKIE[ "language" ] ) ){
      setcookie("language", "en", time() + 3600 * 24 * 14 );
      $english = true;
    }
    
    if( isset( $_GET[ "ru" ] ) ) {
      setcookie("language", "", -1 );
      $english = false;
    }
    
    ob_end_flush();
        
    if( ! isset( $_COOKIE[ "was_here" ] ) )
        $was_here = false;
    
    // get YouTube video link
    global $wpdb;
    
    $link = $wpdb->get_results( 'select value from wp_mm_parameters where name = "youtube_url"' );
    
    if( count( $link ) > 0 )
        $link = $link[ 0 ]->value;
    else
        $link = '';
    
    // get Header    
    get_header();
    
    // get ScreenSize
    $screen_size = $wpdb->get_results( 'select value from wp_mm_parameters where name = "screen_size"' );
    if( count( $screen_size ) > 0 )
        $screen_size = $screen_size[ 0 ]->value;
    else
        $screen_size = 1440;
    
    // user manipulations
    $current_user = wp_get_current_user();
    
    function generateRandomString($length = 6) {
      return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }
    
    //get name + surname
    $avatar_meta = $wpdb->get_results( 'select * from wp_usermeta where ( meta_key = "first_name" or meta_key = "last_name" ) and user_id = ' . $current_user->data->ID );
    $username = '';
    $surname = '';
    if( count( $avatar_meta ) > 0 ){
      foreach( $avatar_meta as $am ) {
        if( $am->meta_key == 'first_name') $username .= $am->meta_value;
        if( $am->meta_key == 'last_name') $surname .= $am->meta_value;
      }      
    }
    if( strlen( $surname ) > 0 ) $username .= " ". $surname;
    if( strlen( $username ) == 0 ) $username = $current_user->data->display_name;
    
    if ( is_user_logged_in() ) {
        $avatar_meta = $wpdb->get_results( 'select * from wp_usermeta where meta_key = "wsl_current_user_image" and user_id = ' . $current_user->data->ID );
        if ( count( $avatar_meta ) > 0 ) {
                                 
            $parse = parse_url( $avatar_meta[ 0 ]->meta_value );
            $path_info = pathinfo( $avatar_meta[ 0 ]->meta_value );
            $path = get_stylesheet_directory() . "/images/avatars/";
                    
            if( $parse[ 'host' ] != "mediamap.me" ){
              
              $name = generateRandomString();
              
              // get avatar from Facebook
              if ( $path_info[ 'basename' ] == "picture?width=150&height=150" ) {
                $ext = "jpg";                
                file_put_contents( $path .$name . ".jpg", fopen( $path_info[ 'dirname' ] . '/' . "picture?width=200&height=200", 'r' ) );
              } else {
                $ext = $path_info['extension'];                
                file_put_contents( $path . $name . "." . $path_info['extension'], fopen( $avatar_meta[ 0 ]->meta_value, 'r' ) );
              }
              
              $avatar = get_template_directory_uri() . "/images/avatars/" . $name . "." . $ext;              
              $wpdb->update( 'wp_usermeta', array( "meta_value" => $avatar ), array( 'umeta_id' => $avatar_meta[0]->umeta_id ), array( '%s') );
            } else {
              $avatar = $avatar_meta[ 0 ]->meta_value;
            }                        
        } else {
            $avatar = "http://mediamap.me/wp-content/themes/mediamap/images/avatar.png";
        }
    }
    
    $lang_name = $english ? 'name_eng': 'name_rus';
    
    // get data
    if ( is_user_logged_in() ) {
      $elements_data = json_encode( $wpdb->get_results( "select e.*, " . $lang_name . " as 'name', coalesce( ue.type, 0 ) as user_state from wp_mm_elements e left outer join ( select * from wp_mm_users_elements where user_id = " . $current_user->data->ID . " ) ue on e.id = ue.element_id  ;" ) );
    } else {
      $elements_data = json_encode( $wpdb->get_results( "select e.*, " . $lang_name . " as 'name', 0 as user_state from wp_mm_elements e ;" ) );
    }
    
?>
<script>
  window.MM = window.MM || { };
  MM.base_screen_size = <?php echo $screen_size; ?>;
  elements_data = <?php echo $elements_data; ?>;
  language = '<?php echo $english ? 'eng': 'rus'; ?>';
</script>
<div class="content">    
    <div class="user_avatar"><?php print_r( $avatar ); ?></div>    
    <div id="header">      
      <nav class="header_content header1">
        <span class="header_content__left <?php if( ! is_user_logged_in() ){ echo "hide"; } ?>">      
          <span class="nickname"><?php print_r( $username );?></span>
          <span class="filter">
            <a class="filter__activate filter-selected"><?php echo $english ? 'All media competence': 'Все медийные компетенции'; ?></a>
            <a class="filter__activate"><?php echo $english ? 'I can': 'Умею'; ?></a>
            <a class="filter__activate"><?php echo $english ? 'Want to know' : 'Хочу научиться'; ?></a>              
          </span>                    
        </span>
        <span class="header_content__right">
          <a href="#" class="show-youtube"><?php echo $english ? 'What that?' : 'Что это?'; ?></a>
          <a class="language" href="<?php echo $english ? '?ru=1': '?en=1'; ?>"> <?php echo $english ? 'Рус': 'Eng'; ?></a>          
          <a href="http://mediamap.me/wp-login.php?action=logout&redirect_to=http%3A%2F%2Fmediamap.me?logout=1" class="logout <?php if( ! is_user_logged_in() ){ echo "hide"; } ?>" title="Logout"><?php echo $english ? 'Logout' : 'Выйти'; ?></a>          
        </span>        
      </nav>
      <?php if( is_user_logged_in() ): ?>
      <nav class="header_content header2 hidden">
        <span class="header_content__left">
          <a href="#" class="save_as_pdf"><?php echo $english ? 'Save PDF' : 'Сохранить PDF'; ?></a>
          <div class="email_step1">
            <span class="send_text"><?php echo $english ? 'Send Email' : 'Отправить на почту'; ?></span>
            <div class="email_input">            
              &nbsp;
              <input type="text" value="email"/>
              <a href="#" class="send_email">&nbsp;&nbsp;&nbsp;&nbsp;</a>          
            </div>
          </div>
          <div class="email_step2 hidden">
            <?php echo $english ? 'Email sent sucessfuly!' : 'Сообщение успешно отправлено!'; ?>
          </div>
          <!--a href="#" class="share"><?php echo $english ? 'Share' : 'Поделиться'; ?></a-->
        </span>
      </nav>
      <?php endif; ?>
    </div>
    <div class="cardlist__shadow c__shadow hidden"></div>
    <div class="cardlist__popup hidden">      
      <div class="row"></div>
      <img class="close" src="http://mediamap.me/wp-content/themes/mediamap/images/close-card.png">
      <div class="cardlist__content">        
        <div class="buttons <?php if( ! is_user_logged_in() ) echo "hidden"; ?>">
          <span class="can"><?php echo $english ? 'I can': 'Умею'; ?></span>
          <span class="want"><?php echo $english ? 'Want to know' : 'Хочу научиться'; ?></span>
        </div>        
        <div class="cardlist">
          <div class="card_preview">            
            <div class="card_preview__action">Сделайте это</div>
            <div class="card_preview__title">Название этой замечательной карточки находится именно здесь</div>
          </div>
          <div class="card_preview">            
            <div class="card_preview__action">Сделайте это</div>
            <div class="card_preview__title">Название этой замечательной карточки находится именно здесь</div>
          </div>
          <div class="card_preview">            
            <div class="card_preview__action">Сделайте это</div>
            <div class="card_preview__title">Название этой замечательной карточки находится именно здесь</div>
          </div>
          <div class="card_preview">            
            <div class="card_preview__action">Сделайте это</div>
            <div class="card_preview__title">Название этой замечательной карточки находится именно здесь</div>
          </div>
          <div class="card_preview">            
            <div class="card_preview__action">Сделайте это</div>
            <div class="card_preview__title">Название этой замечательной карточки находится именно здесь</div>
          </div>
          <div class="card_preview">            
            <div class="card_preview__action">Сделайте это</div>
            <div class="card_preview__title">Название этой замечательной карточки находится именно здесь</div>
          </div>
        </div>
      </div>      
    </div>    
    <div class="card__popup hidden">
      <div class="card__shadow c__shadow hidden"></div>
      <div class="card__wrap">
        <div class="row"></div>
        <img class="close-card" src="http://mediamap.me/wp-content/themes/mediamap/images/close-card.png">
        <div class="card__content">
          <div class="path">Публикация/работа на платформах → Публикация/выдача в эфир</div>
          <div class="title">ОРГАНИЗАЦИЯ ПРЯМОЙ ТРАНСЛЯЦИИ</div>
          <div class="content">
            Обычное дело: 85 человек пришли на лекцию вживую, а ещё 2 300 смотрят в онлайне. Трансляция видео на сайт – это простой способ увеличить охват аудитории на лекциях, форумах, семинарах и тренингах. Перестаньте игнорировать людей из других городов и стран. Транслируйте свои мысли на весь мир!
          </div>
          <div class="sharing">
            <div class="sharing__row"><?php echo $english ? 'SHARE' : 'ПОДЕЛИТЬСЯ'; ?></div>
            <div class="sharing__row">
              <a class="facebook" href="#"><img src="http://mediamap.me/wp-content/themes/mediamap/images/facebook-share.png"></a>
              <!--<a class="twitter" href="#"><img src="http://mediamap.me/wp-content/themes/mediamap/images/twitter-share.png"></a>-->
              <a class="vk" href="#"><!--<img src="http://mediamap.me/wp-content/themes/mediamap/images/vk-share.png">--></a>               
            </div>
          </div>
        </div>
      </div>  
    </div>        
    <canvas id="myCanvas"></canvas>
    <?php if( ! is_user_logged_in() ): ?>
        <div class="auth__popup popup">
            <div class="auth__content popup__content">        
                <?php do_action( 'wordpress_social_login' ); ?>            
            </div>
        </div>
    <?php endif;?>
    <div class="youtube__popup popup <?php if( is_user_logged_in() || $was_here ) echo "hidden"; ?>">
        
        <div class="youtube__shadow">          
        </div>
        <div class="youtube__content popup__content">
            <img class="close" src="http://mediamap.me/wp-content/themes/mediamap/images/close-popup-hover.png">
            <iframe id="youtube__player" type="text/html" width="720" height="405"
                src="<?php echo $link; ?>"
                frameborder="0" allowfullscreen>            
        </div>        
    </div>
</div>
<?php get_footer(); ?>
