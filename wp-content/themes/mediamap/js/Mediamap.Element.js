window.MM = window.MM || { };

/**
 *		Element Class
 */
MM.Element = function () {
	
	this.id = false;
	this.db_id = false;
	this.Dot = false;
	this.Stroke = false;
	this.Label = false;
	this.level = false;
	
	this.parent = false;
	this.children = [];
	this.user_state = 0;

	this._default_color = "white";
	
	// Element initialization
	this.init = function ( data ){
				
		if ( typeof( data ) === 'undefined' ) {
            data = {};
        }
		
		if ( data.hasOwnProperty( 'db_id' ) ) {
			this.db_id = data.db_id;
		} else {
			this.db_id = 0;
		}		
		
		if ( data.hasOwnProperty( 'user_state' ) )
			this.user_state = data.user_state;
			
		// set default position for root
		if ( ! data.hasOwnProperty( 'dot_position' ) ) {            
			data.dot_position = { x : $('#myCanvas').width() / 2, y : $('#myCanvas').height() / 2 };	
		}		
						
		//add parent
		if ( data.hasOwnProperty( 'parent' ) ) {		
            this.parent = data.parent;
			this.parent.children.push( this );
		}
		
		// set default color
		if ( data.hasOwnProperty( 'color' ) ) {
			this._default_color = data.color;
		} else {
			if ( this.level == 1 ) {
                this._default_color = MM.Elements.getColor();
            }
			if (this.level > 1 ) {
				this._default_color = this.parent._default_color;
			}
		}
		
		// set level
		if ( ! this.parent ) {
            this.level = 0;
        } else {
			this.level = this.parent.level + 1;
			
			if ( MM.Elements.top_level < this.level ) {
                MM.Elements.top_level = this.level;
            }
		}
		
		// set style
		this.style = MM.ElementStyle.style( this.level );
		
		// draw Dot
		this._drawDot( data.dot_position );
		
		// draw Stroke
		if ( data.hasOwnProperty( 'stroke_handle1_position' ) ) {
			if ( data.hasOwnProperty( 'hexagon_point' ) ) {
				this._drawStroke( data.stroke_handle1_position, data.stroke_handle1_position, data.hexagon_point );
			} else {
				this._drawStroke( data.stroke_handle1_position, data.stroke_handle1_position, false );
			}
		} else {
			this._drawStroke( );
		}
		
		// draw Label		
		if ( data.hasOwnProperty( 'title' ) ){
			
			if ( data.hasOwnProperty( 'label_position' ) ) {
                var label_position = data.label_position;
            }else{
				var label_position = data.dot_position;
			}
			
			this._drawLabel( data.title, label_position );				            
		}
						
		if ( data.hasOwnProperty('display_children') ) {            
            this.display_children = data.display_children;            
		}
		
		if ( this.parent.display_children ) {
            this.display_children = true;        
        }	
		if ( ! this.display_children && this.level != 1 ) {
			this.Dot.visible = false;
			this.Label.visible = false;
			this.Stroke.visible = false;
        }
		// add ID of the selected branch
		if ( this.display_children && this.level == 1 ) {
            MM.Elements.selected_branch_first_element_db_id = this.db_id;
        }
				
		// save object changes
		this.saveToElements();
	}
	
	// save element
	this.saveToElements = function () {
        MM.Elements.save( this );
	}

	// remove element
	this.deleteFromElements = function () {
		MM.Elements.remove( this );		
	}
	
	// Set Base Colors
	this.setBaseColors = function () {
		this.Dot.strokeColor = this._default_color;
		this.Dot.fillColor = this._default_color;
		
		if ( this.Label ) {
            this.Label.fillColor = this._default_color;
        }
		
		if ( this.Stroke ) {
            this.Stroke.strokeColor = this._default_color;
        }
	}
	
	// Draw Dot
	this._drawDot = function ( position ) {
							
		var _this = this;
		
		if ( ! _this.parent ) {
			var radius = 10;    
        } else {
			var radius = 3;
		}
				
		this.Dot = new Path.Circle( {
			center: new Point( parseInt( position.x ), parseInt( position.y ) ),
			radius: radius,
			strokeColor: this._default_color,
			fillColor: this._default_color,
		}).attach('click', function () {
			if ( ! _this.parent  ) {			
                MM.Elements.selected_level = 0;				
                MM.Canvas.move( MM.centerpoint, 0.7 );
            } else {
				_this._onClickLabel( _this );
			}
		});
				
		this.Dot.onMouseEnter = function ( ) {
			$("html").css("cursor","pointer");
		}
		
		this.Dot.onMouseLeave = function ( ) {
			$("html").css("cursor","default");
		}
	};
	
	// Draw Stroke
	this._drawStroke = function ( h1, h2, hexagon_point ) {
		
		if ( ! this.parent )
            return;
        
		var start = new Point( this.parent.Dot.position.x, this.parent.Dot.position.y );
						
		if ( this.level == 1 ) {
			start = MM.Hexagon.polygon.segments[ hexagon_point - 1 ].point;   
        }
		
		var end = new Point( this.Dot.position.x, this.Dot.position.y );
				
		var handleX = ( start.x + end.x ) / 2 - start.x + Math.cos( start.angle ) *
				Math.sqrt( Math.pow( end.x - start.x, 2 ) + Math.pow(  end.y - start.y, 2 ) )/2;
		var handleY = ( start.y + end.y ) / 2 - start.y + Math.sin( start.angle ) *
				Math.sqrt( Math.pow( end.x - start.x, 2 ) + Math.pow( end.y - start.y, 2 ) )/2;		
		
		var segments = [
			new Segment( {
				point: start,
			    handleOut: [ handleX, handleY ]
			}),
			new Segment( {
				point: end,
			    handleIn: [ 0, 0 ]
			}),            
            //end                			
		];
		
		var _this = this;
				
		var path = new Path( segments ).attach( 'click', function () {			
		});
		
		if ( typeof( h1 ) !== 'undefined' ) {
			path.firstCurve.handle1.x = parseInt( h1.x );
			path.firstCurve.handle1.y = parseInt( h1.y );
			path.lastCurve.handle2.x = parseInt( h2.x );
			path.lastCurve.handle2.y = parseInt( h2.y );
        }
		
		path.strokeColor = _this._default_color;
		
		path.strokeWidth = _this.style.strokeWidth;
		path.opacity = _this.style.strokeOpacity;		 
				        
		this.Stroke = path;
        this.Stroke.insertBelow( MM.Hexagon.polygon );
		
	};
	/*	Small function that colorize Dot, Label, Stroke to some coler 
	 */
	this.colorize = function ( color ) {
										
		//fill Stroke
		this.Stroke.strokeColor = color;
		//fill Lable
		this.Label.fillColor = color;
		
		// fill Dot
		this.Dot.strokeColor = color;
		this.Dot.fillColor = color;
	}
	
		
	// on click Label
	this._onClickLabel = function ( _this, callback ) {
		MM.Elements.selected_level = _this.level;		
			
		if ( _this.level > 0 && ( _this.children.length > 0 ) ) {
			
			var bc = MM.Canvas.branchCenter( _this );
							
			if (_this.level == 1 ){
				_this.displayBranch( _this );
				MM.Elements.selected_branch_first_element_db_id = _this.db_id;        
			}
			
			/*	Move canvas and zoom it. If there is a callback move canvas without animation
			 */
			if ( typeof( callback ) !== 'undefined' ) {
				MM.Canvas.move( bc.point, bc.value, true );
			} else {
				MM.Canvas.move( bc.point, bc.value);
			}
        }
		
		if ( _this.children.length == 0 ) {
			MM.CardList.pin( _this );
		} else {
			MM.CardList.unpin();
		}
		
		/*	Fill line with white and add back button to all parents element
		 */
		MM.Elements.restoreColors();
		
		// fill current element tree with white
		var element_parent = _this;		
		
					
		// colorize Dot, Stroke and Label
		element_parent.colorize( 'white' );
								
		// add Return			
		element_parent.Label.data.Return = new PointText( {
			point : [ element_parent.Label.bounds.left, element_parent.Label.bounds.bottom + 10 ],
			content: "на шаг назад",
			fillColor: "white",
			fontFamily: 'PT Sans',
			fontSize:  parseFloat( element_parent.Label.fontSize.replace('px', '') )*0.75 + "px",
			opacity: "0.4",
		}).attach( 'click', function( ){				
			if ( this.data.element.parent.db_id != 0 ){
				this.data.element.parent._onClickLabel( this.data.element.parent );
			} else {
				MM.Hexagon.onClick();				
			}
		});
		
		element_parent.Label.data.Return.onMouseEnter = function ( ) {
			$("html").css("cursor","pointer");
		}
		
		element_parent.Label.data.Return.onMouseLeave = function ( ) {
			$("html").css("cursor","default");
		}
		
		element_parent.Label.data.Return.data.element = element_parent;									
		
		MM.Elements.selected_element = _this;
		
		// run callback
		if ( typeof( callback ) !== 'undefined' ) {
            callback();
        }
	}
	
	this.convertPositionX = function ( Lax ) {
		return paper.view.projectToView( Lax ).x;
	}
	
	this.convertPositionY = function ( Lax ) {
		return paper.view.projectToView( Lax ).y;
	}
	
	// Draw Label
	this._drawLabel = function ( title, label_position ) {
		
		var _this = this;
				
		this.Label = new PointText( {
			point : [ this.Dot.position.x + 5, this.Dot.position.y + 5 ],
			content: title,
			fillColor: this._default_color,
			fontFamily: 'PT Sans',
			fontSize: _this.style.labelFontSize,						
		}).attach( 'click', function( ){ _this._onClickLabel( _this ); } );
		
		_this.Label.opacity = _this.style.labelOpacity,
		
		this.Label.bounds.x = label_position.x;
		this.Label.bounds.y = label_position.y;
        		
		// add Underline
		_this.Label.data.Underline = new Path.Line(
			new Point( _this.Label.bounds.left, _this.Label.bounds.bottom ),
			new Point( _this.Label.bounds.right, _this.Label.bounds.bottom )
		);
		
		_this.Label.data.Underline.strokeColor = _this._default_color;
		_this.Label.data.Underline.opacity = 0.5;
		_this.Label.data.Underline.visible = false;
				
		// On Mouse Enter		
		this.Label.onMouseEnter = function ( ) {
			
			$("html").css("cursor","pointer");
			
			if ( ! $( ".cardlist__popup" ).hasClass( "hidden" ) || ! $( ".cardlist__popup" ).hasClass( "hidden" ) ) {
                return;
            }
			
			if ( MM.Elements.selected_level == 0 && _this.level == 1 ){
				MM.Elements.selected_branch_first_element_db_id = _this.db_id;
				_this.displayBranch( _this );
			}
			
			_this.Label.data.Underline.visible = true;
			_this.Label.data.Underline.strokeColor = _this.Label.fillColor;
			
			//
			if ( _this.level > 1 )
				_this.Label.opacity = 1;
			
			// show cardlist			
			if ( _this.children.length == 0 ) {
				_this.Label.data.show_cardlist = true;
				setTimeout( function(){
					if ( _this.Label.data.show_cardlist ) {
						MM.CardList.show( _this );						    
                    }
				} , 300);

				
			}								
		}
		
		this.Label.onMouseLeave = function ( event ) {
			
			$("html").css("cursor","default");
			
			if ( _this.level > 1 )
				_this.Label.opacity = _this.style.labelOpacity;
			
			_this.Label.data.show_cardlist = false;
			
			setTimeout( function(){
					if ( ! MM.CardList.mouseover ) {
						MM.CardList.hide();
                    }
				} , 300);
			
						
			_this.Label.data.Underline.visible = false;
		}
				
	};	
	//	Display Branch
	this.displayBranch = function ( branch, state ) {
						
		if ( typeof( state ) === 'undefined' ) {
            state = $('.filter-selected').index();
        }
				
		var display_list = [];
				
		for( var i in MM.Elements.storage ){
			
			var el = MM.Elements.storage[ i ];
			
			if ( state != 0 && el.user_state == state ) {				
				display_list.push( el.id );				
				display_list = MM.Elements.getElementTree( el.parent.id, display_list );
			}
			
			if ( el.level > 1 ) {
				el.Label.visible = false;
				el.Dot.visible = false;
				el.Stroke.visible = false;
			}						
		}
						
		function display_me_and_children( el, state, display_list ) {
			
			if ( el.level == 1 || $.inArray( el.id, display_list ) >= 0 || state == 0 ) {
				el.Label.visible = true;
				el.Dot.visible = true;
				el.Stroke.visible = true;
            }
			
			for( var i = 0; i < el.children.length; i++ ) {
				display_me_and_children( el.children[ i ], state, display_list );
			}
		}
		
		display_me_and_children( branch, state, display_list );
		paper.view.update();	
	}
	
}