window.MM = window.MM || { };

MM.client_init = function ( elements_data, callback ){
    	
    // initialize Elements
    var element = new MM.Element();
    element.init();
		
	var display_children = true;
	
	var dt = elements_data;
	
	MM.Canvas.initial_center = paper.view.center;
	MM.Hexagon.init();
	MM.CardList.init();
	MM.Card.init();
	
	for ( var i = 0; i < dt.length; i++ ) {
								
		var el = new MM.Element();
		
		el.init( {
			'db_id' : dt[ i ].id,
			'dot_position' : {
				x: MM.absoluteWidth( dt[ i ].dot_x ),
				y: MM.absoluteHeight( dt[ i ].dot_y )
			},
			'label_position' : {
				x: MM.absoluteWidth( dt[ i ].label_x ),
				y: MM.absoluteHeight( dt[ i ].label_y )
			},				
			"stroke_handle1_position" : {
				x: MM.absoluteWidth( dt[ i ].stroke_handle1_x ),
				y: MM.absoluteHeight( dt[ i ].stroke_handle1_y )
			},
			"stroke_handle2_position" : {
				x: MM.absoluteWidth( dt[ i ].stroke_handle2_x ),
				y: MM.absoluteHeight( dt[ i ].stroke_handle2_y )
			},
			"stroke_start" : {
				x: MM.absoluteWidth( dt[ i ].stroke_start_x ),
				y: MM.absoluteHeight( dt[ i ].stroke_start_y )
			},
			"hexagon_point" 			: dt[ i ].hexagon_point,
			'parent' 					: MM.Elements.getElementByID( dt[ i ].parent_id ),
			'title' 					: dt[ i ].name,
			'color' : dt[ i ].color,
			'display_children' : display_children,
			'user_state' :  dt[ i ].user_state
		} );
		
		var display_children = false;
	}
	
	paper.view.update();
	
	// Run callback
	if( typeof( callback ) !== 'undefined' )
		callback();
}

// Queue
$.request = (function () {
	var queue = $.Deferred().resolve();

	return function (options) {
		function _call () {
			return $.ajax(options);
		}

		return (queue = queue.then(_call, _call));
	};
})();
		
/**
 *	Ajax
 */
MM.Ajax = MM.Ajax || { };
	
// Get Elements
MM.Ajax.getElements = function ( callback ) {
		
	var ans = [];
	
	$.ajax( {
		type: "GET",
		url: "wp-admin/admin-ajax.php",
		data: {
			action : 'getElements'
		},
		success: function ( response ) {							
			callback( response );								
		}
	} );				
}
/*
 *
 */
MM.Ajax.getPDF = function ( newWindow, callback ) {
	
	var type = $(".filter-selected").index();
	
	if ( type < 1 || type > 2 )
        return;
	
	$.ajax( {
		type: "POST",
		url: "wp-admin/admin-ajax.php?action=pdf",
		data: JSON.stringify( { type : type } ),
		success: function ( response ) {
			newWindow.location.href = response;			
		}
	} );			
}
/*
 *
 */
MM.Ajax.sendEmail = function ( callback ) {
	
	var type = $(".filter-selected").index();
	
	if ( type < 1 || type > 2 )
        return;
	
	$.ajax( {
		type: "POST",
		url: "wp-admin/admin-ajax.php?action=sendEmail",
		data: JSON.stringify( { type : type } ),
		success: function ( response ) {
			callback();
		}
	} );			
}

/*
 *	Save element state	
 */
MM.Ajax.saveElementState = function ( data, callback ) {
	
	MM.Elements.status_queue.push( data );
	
	var finish = true;
			
	function send_data () {
		
		var _data = [];
		
		finish = false;
		
		for( var i = 0; i < MM.Elements.status_queue.length ; i++ ) {
			var el = MM.Elements.status_queue.pop();
			if ( $.inArray( el, _data ) == -1 ) {
				_data.push( el );
			}
		}
				
		$.ajax( {
			type: "POST",
			url: "wp-admin/admin-ajax.php?action=userState",				
			data: JSON.stringify( _data ),
			success: function ( response ) {							
				if ( MM.Elements.status_queue.length != 0 ) {
					send_data();
				} else {
					finish = true;
				}
			}
		} );				
	}
	
	if ( finish ) {
		send_data();
	}
	
}	


/**
 *		Elements Class
 */
MM.Elements = new function( ){
	
	this.selected_level = 0;
	this.selected_branch_first_element_db_id = false;
	this.selected_element = false;
	this._last_id = 0;
	this.storage = {};	
	this.status_queue = [];
	
	this.newID = function( ) {
		return ++this._last_id;
	}
	
	this.save = function( el ){
		if ( ! el.id ) {
            el.id = this.newID();
        }		
		this.storage[ el.id ] = el;
	}
	
	this.remove = function( el ){
		delete this.storage[ el.id ];
	}
	
	this.getElementByID = function ( id ){
		
		for ( var p in this.storage ) {
			if ( this.storage.hasOwnProperty( p ) ) {				
				if ( this.storage[ p ].db_id == id ) {					
					return this.storage[ p ];
				}
			}
		}
	}
	/*
	 *	Blur all 1 level elements, except el
	 */
	this.blurFirstElements = function ( args ) {
		
		var opacity = 0.5;
		var el = false;
		
		if ( args.hasOwnProperty( 'opacity' ) )
            opacity = args.opacity;
        
		if ( args.hasOwnProperty( 'element' ) )
            el = args.element;
				
		for ( var k in MM.Elements.storage ){
			var e = MM.Elements.storage[ k ];
			if ( e.level == 1 ) {				
				if ( ( el && e.id != el.id ) || ! el ) {					
					e.Label.opacity = opacity;
				}
			}
		}
		
		if ( el ) {
			el.Label.opacity = 1;
		}
	}
	
	/*
	 * 	Get Element Parent three
	 */
	this.getElementTree = function ( el_id, array ) {
		
		for( var i in MM.Elements.storage ){
			
			if ( MM.Elements.storage[ i ].id == el_id ) {										

				if( $.inArray( el_id, array ) == -1 ){
					array.push( MM.Elements.storage[ i ].id );						
				}

				if ( MM.Elements.storage[ i ].level > 1 ) {						
					array = MM.Elements.getElementTree( MM.Elements.storage[ i ].parent.id, array );
				}
				break;
			}
			
		}
		
		return array;
    }
	/*	Fill line with white and add back button to all parents element
	*/
	this.restoreColors = function () {
	
		if ( MM.Elements.selected_element ) {
                    
			var element_parent = MM.Elements.selected_element;
			
			// fill previous element with thier color
			while ( true ) {
				// colorize Dot, Stroke and Label
				element_parent.colorize( element_parent._default_color );
				
				//remove Return
				if ( typeof( element_parent.Label.data.Return ) !== 'undefined' ) {
					element_parent.Label.data.Return.remove();
				}
							
				// rule to break
				if ( element_parent.parent && element_parent.parent.db_id != 0 ){				
					element_parent = element_parent.parent;
				} else {
					break;
				}
			}
		}
		
		MM.Elements.makeFirstTransperently();
	}
	
	this.makeFirstTransperently = function () {
		
		if ( $( ".filter-selected" ).index() == 0 ) 
            return;
        
		// function
		function  check_element_status( el, state ) {

			var hidden_children = true;
			
			if ( el.children.length == 0 )
                return true;
            			
			for ( var j in el.children ) {
				if ( parseInt(el.children[ j ].user_state) == state ) {					
					return false;
				} else {
					if ( ! check_element_status( el.children[ j ], state ) )
						return false;
				}
			}
			
			return hidden_children;		
        }
		
		// Make first level elements transparent if their don't have child after filter
		for ( var i in MM.Elements.storage ) {
			
			var el = MM.Elements.storage[ i ];
			
            if ( el.parent.db_id == 0 ) {
				
				var hidden_children = check_element_status( el, $( ".filter-selected" ).index() );
												
				if ( hidden_children ) {
                    el.Label.opacity = 0.5;
                } else {
					el.Label.opacity = 1;
				}
			}
		}
		
		paper.view.update();
	}
};
/*
 *	Card List
 */
MM.CardList = new function () {
	
	this.isPinned = false;
	this.element = false;
	this.data = false;
	this.mouseover = false;
	
	// init
	this.init = function () {
		
		var _this = this;
		
		$( ".cardlist__shadow, .cardlist__popup .close" ).on( 'click', function () {
			MM.CardList.unpin();
		});
		
		$( ".cardlist__content .buttons span" ).on( 'click', function () {
						
			var state = 0;
			
			if ( $( this ).hasClass( "can" ) ) {
				state = 1;				
			}
			
			if ( $( this ).hasClass( "want" ) ) {
				state = 2;
			}
			
			$( ".cardlist__content .buttons span" ).each( function () {
				$( this ).css({
					"border-color" : _this.element._default_color,
					"background" : "white",
					"color" : "black"
				});
			});
			
			if ( _this.element.user_state == state ) {
                state = 0;				
            } else {
				$( this ).css({
					"border-color" : _this.element._default_color,
					"background" : _this.element._default_color,
					"color" : "white"
				});
			}
			
			_this.element.user_state = state;
			
			MM.Ajax.saveElementState( { id : _this.element.db_id, state : _this.element.user_state } );
									
		});
		
		$(".cardlist__popup").mouseenter( function () {			
			_this.mouseover = true;
		} ).mouseleave( function () {
			if ( ! _this.isPinned ) {
                _this.hide();
            }
			_this.mouseover = false;
		} );
	}
	
	// Pin CardList
	this.pin = function ( _this ) {		
		this.isPinned = false;	
		this.show( _this, true );
	}
	
	// Unpin CardList
	this.unpin = function ( ) {		
		this.isPinned = false;
		this.hide();		
		$( ".cardlist__shadow" ).addClass( "hidden" );
	}
	
	// OnClick Card Preview
	this.onClickPreview = function () {
		if ( ! MM.CardList.isPinned ) {
                MM.CardList.pin( MM.CardList.element );
            }
		MM.Card.show(
			MM.CardList.data[
				$(this).parent().index()
			]
		);
	}
	
	// Show CardList
	this.show = function ( _this, pin ) {
		
		if ( this.isPinned )
			return;        
		
		if ( this.element != _this ) {                    
			this.element = _this;
			
			// get Elements
			MM.Ajax.getCards( _this.db_id, function ( res ) {
				$(".cardlist").html('');
				
				MM.CardList.data = res;
							
				for( var i in res ){
					
					var el = $('<div class="card_preview"></div>');					
					
					var action = res[i][ "action_" + language ];
					var title = res[i][ "name_" + language ];
					var content = res[i][ "text_" + language ];
					
					if ( action.length == 0 && title.length == 0 && content.length == 0 ) {
                        continue;
                    }
					
					if ( action.length == 0 ) action = 'untitled';
					if ( title.length == 0 ) title = 'unnamed';
										
					$( '<div></div>' ).html( action ).addClass( "card_preview__action" ).appendTo( el ).on( 'click', MM.CardList.onClickPreview );
					$( '<div></div>' ).html( title ).addClass( "card_preview__title" ).appendTo( el ).on( 'click', MM.CardList.onClickPreview );
					
					el.appendTo(".cardlist");
									
				}
				
				$( ".cardlist__popup .card_preview__action" ).css( "color", _this._default_color );
				
				MM.CardList._real_show( _this, pin );

			}, 1 );
			
		} else {
			MM.CardList._real_show( _this, pin );
		}	
	}
	
	/*
	 *	Real show function
	 */
	this._real_show = function ( _this, pin ) {
		// show CardList
		
		var lr = "_left";
		var tb = "_top";
		
		// определить координаты x				
		if ( _this.convertPositionX( _this.Label.bounds.right ) + 30 + $( ".cardlist__popup" ).width() + 10 < $( window ).width() ) {
			// show block right
			$( ".cardlist__popup" ).css( "left", _this.convertPositionX( _this.Label.bounds.right ) + 30 );										
		} else {
			$( ".cardlist__popup" ).css( "left", _this.convertPositionX( _this.Label.bounds.left ) - 30 - $( ".cardlist__popup" ).width() );
			lr = "_right";
		}
		
		if ( _this.convertPositionY( _this.Label.bounds.top ) + $( ".cardlist__popup" ).height() < $( window ).height() ) {
			$( ".cardlist__popup" ).css( "top", _this.convertPositionY( _this.Label.bounds.top ) - 15 );					
		} else {
			$( ".cardlist__popup" ).css( "top", _this.convertPositionY( _this.Label.bounds.bottom ) + 15 - $( ".cardlist__popup" ).height() );
			tb = "_bottom";
		}
		
		$( ".cardlist__popup" ).removeClass( "cardlist__popup_right_bottom" );
		$( ".cardlist__popup" ).removeClass( "cardlist__popup_left_bottom" );
		$( ".cardlist__popup" ).removeClass( "cardlist__popup_right_top" );
		$( ".cardlist__popup" ).removeClass( "cardlist__popup_left_top" );
		
		$( ".cardlist__popup" ).addClass( "cardlist__popup" + lr + tb );
		
		$( ".cardlist__popup .row" ).css( "background", _this._default_color );
		$( ".cardlist__popup .buttons span" ).css( "border-color", _this._default_color );		
		
		$( ".cardlist__content .buttons span" ).css({
				"border-color" : _this._default_color,
				"background" : "white",
				"color" : "black"
			});
		
		if ( _this.user_state == "1")
			$( ".cardlist__content .buttons .can" ).css({
				"border-color" : _this._default_color,
				"background" : _this._default_color,
				"color" : "white"
			});
		
		if ( _this.user_state == "2")
			$( ".cardlist__content .buttons .want" ).css({
				"border-color" : _this._default_color,
				"background" : _this._default_color,
				"color" : "white"
			});		
		
		// Show card list only for auth users and if there are cards inside		
		if ( $( ".cardlist > div").length > 0 || ! $( ".cardlist__popup .buttons" ).hasClass( "hidden" ) ) {
			$( ".cardlist__popup" ).removeClass( "hidden" );
			
			if ( typeof( pin ) !== 'undefined' ) {
				this.isPinned = true;		
				$( ".cardlist__shadow" ).removeClass( "hidden" );
			}
		}
	}
	
	// Hide CardList
	this.hide = function () {
		
		if ( this.isPinned )
            return;
        
		$( ".cardlist__popup" ).addClass( "hidden" );
	}
}

/*
 *	Card
 */
MM.Card = new function () {
	
	// last selected card data
	this.data = false;
	
	/*
	 *	Show Card
	 */
	this.show = function ( res ) {
		
		this.data = res;
		
		// show path
		var path_items	= MM.Elements.getElementTree( MM.CardList.element.id, [] )
			path_html 	= "",
			color 		= 'black';
		
		for ( var i = path_items.length - 1; i >= 0; i -- ) {
			
			for ( var j in MM.Elements.storage ) {
				
				if ( MM.Elements.storage[ j ].id == path_items[ i ] ) {
                    
					if ( path_html.length > 0 ) {
                        path_html += " &#8594; ";
                    }
										
                    path_html += MM.Elements.storage[ j ].Label.content;
					color = MM.Elements.storage[ j ]._default_color;
                }				
			}			
		}
		
		$( ".card__content > .path" ).html( path_html ).css( "color", color );
		
		// line background
		$( ".card__wrap > .row" ).css( "background", color );
		
		// Show path title and description
		if ( language == 'rus' ) {
			$( ".card__content > .title" ).html( res.name_rus );
			$( ".card__content > .content" ).html( res.text_rus );
        } else {
			$( ".card__content > .title" ).html( res.name_eng );
			$( ".card__content > .content" ).html( res.text_eng );
		}				
		
		// Prepare VK button		
		var data = MM.Card.prepareShareData( "vk" );
						
		data.url = encodeURIComponent( data.url );
		
        var a_html = VK.Share.button( data, {type: 'custom', text: "<img src=\"http://vk.com/images/share_32_eng.png\" width=\"26\" height=\"26\" />"} );
        		
		// add button
		$(".card__popup .vk").html( a_html );
		
						
		// Show Card popup and shadow
		$( ".card__popup, .card__shadow" ).removeClass( "hidden" );
				
	}
	
	/*
	 *	Prepare data for share in VK and FB
	 */
	this.prepareShareData = function ( type ) {
		
		var data = {
			method: 'feed',
			link: 'http://mediamap.me',		
			picture: 'http://mediamap.me/wp-content/themes/mediamap/images/mm-fb.png',
			image: 'http://mediamap.me/wp-content/themes/mediamap/images/mm-fb.png',
			title: 'MediaMap.me интерактивная карта медиакомпетенций',
			name: 'MediaMap.me интерактивная карта медиакомпетенций',
			description: "Навигатор по навыкам мира медиа",
			caption: 'mediamap.me',		
		}
		
		// If there is any picture in text, set it to data			
		var image_result = $( ".card__content .content img" );
		
		if ( image_result.length > 0 ) {
			data[ "picture" ] = $( ".card__content .content img" ).first().attr( "src" );
			data[ "image" ] = $( ".card__content .content img" ).first().attr( "src" );
		}
				
		// Make href that can open card, when user return
		if ( type == "fb" ){						
			data[ "link" ] += "/?element=" + MM.CardList.element.db_id + "&card=" + MM.Card.data.id;
		} else {
			data[ "url" ] = data.link + "/?element=" + MM.CardList.element.db_id + "&card=" + MM.Card.data.id;
		}
				
		// Set title & caption
		if ( language == 'rus' ) {
			data[ 'name' ] = MM.Card.data.action_rus + " " + MM.Card.data.name_rus;			
			data[ 'title' ] = MM.Card.data.action_rus + " " + MM.Card.data.name_rus;			
		} else {
			data[ 'name' ] = MM.Card.data.action_eng + " " + MM.Card.data.name_eng;
			data[ 'title' ] = MM.Card.data.action_eng + " " + MM.Card.data.name_eng;
		}						
	
		//Set description
		data[ 'description' ] = $( ".card__content .content" ).text().substring(0, 30);
		
		return data;
	}
	
	/*
	 *	Init function
	 */
	this.init = function () {
		
		var _this = this;
				
		$( ".card__popup .close-card" )
		/*
		 *  On hover Card 
		 */		
		.mouseenter( function () {			
			$( ".card__popup .close-card" ).attr( "src", "/wp-content/themes/mediamap/images/close-card-hover.png" ); })
		.mouseleave( function () {
			$( this ).attr( "src", "/wp-content/themes/mediamap/images/close-card.png" ); })
		/*
		 *	Close Card
		 */
		.click( _this.closeCard );
		
		$( ".card__shadow").click( _this.closeCard );
		
		// Share Facebook
		$( ".card__popup .facebook" ).click( function (){
				
			// Call Facebook
			FB.ui( MM.Card.prepareShareData( 'fb' ) , function(response){} );
		});
	}
	
	/*
	 *	Close Card
	 */
	this.closeCard = function () {		
		$( ".card__content > .title, .card__content > .path, .card__content > .content" ).html("");
		$( ".card__popup, .card__shadow" ).addClass( "hidden" );		
	}
}