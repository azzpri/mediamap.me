YouTube = new function () {
	
	this.el = {
		show	: '.show-youtube',
		popup	: '.youtube__popup',
		shadow	: '.youtube__shadow',
		content : '.youtube__content',			
	};
			
	// Init
	this.init = function () {
		
		// show popup
		$( this.el.show ).on( 'click', function () {
			$( YouTube.el.popup ).show();
		});				
		
		//hide popup		
		$( this.el.shadow + ", .youtube__content .close" ).on( 'click', function () {			
			$( YouTube.el.popup ).hide();			
			$( "#youtube__player" )[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');    
		});		
					
	}
}

window.fbAsyncInit = function() {
		FB.init({
		  appId      : '1560706107572908',
		  xfbml      : true,
		  version    : 'v2.5'
		});
	  };

	(function(d, s, id){
	   var js, fjs = d.getElementsByTagName(s)[0];
	   if (d.getElementById(id)) {return;}
	   js = d.createElement(s); js.id = id;
	   js.src = "//connect.facebook.net/en_US/sdk.js";
	   fjs.parentNode.insertBefore(js, fjs);
	 }(document, 'script', 'facebook-jssdk'));
	
paper.install(window);

$( document ).ready( function () {

	// init YouTube popup	
	YouTube.init();
	
	// Initialize MediaMap plugin    
    paper.setup('myCanvas');		
	MM.init( function () {
		MM.client_init( elements_data, function () {
										
			var get_values = { "element" : false, "card" : false };

			document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
				function decode(s) {
					return decodeURIComponent(s.split("+").join(" "));
				}
			
				get_values[ decode( arguments[1] ) ] = decode( arguments[2] );
			});
			
			if ( ! get_values.element || ! get_values.card ) return;			

			for( var i in MM.Elements.storage ){
				if ( MM.Elements.storage[i].db_id == get_values.element ) {
					el = MM.Elements.storage[i];
					break;
				}
			}
				
			if( !el ) return;	
			
			/*	Find element parent and display branch for it
			 */	
			var element_parent = el;
			while ( true ) {
				if ( element_parent.parent && element_parent.parent.db_id != 0 ){
					element_parent = element_parent.parent;
				} else {
					break;
				}
			}
			
			// Quit if element_parent = false, mean that we enter first-level element id
			if ( ! el == element_parent ) return;
									
			// Display parent element		
			element_parent.Label.onMouseEnter();
			
			/*	Show element, cardlist
			 */
			el.parent._onClickLabel( el.parent, function () {
				
				el._onClickLabel( el, function () {						
		
					// Show card					
					var timerId = setInterval( function() {
					
						if ( MM.CardList.data.length > 0 ) {
                            clearInterval(timerId);
							
							for ( var i in MM.CardList.data ) {
								if ( MM.CardList.data[ i ].id == get_values.card  ) {
                                    MM.Card.show( MM.CardList.data[ i ] );
									break;
                                }
							}
														
                        }

					}, 200);
					
				});								
			});
										
		});
	});
		
	// Update paper
	paper.view.update();
	
	return;
	
	//hide auth
	$( '.auth__popup' ).on( 'click', function () {
		$( '.auth__popup' ).hide();
	});
	
	//activate filter
	$( '.filter__activate' ).on( 'click', function () {
		$( '.filter__activate' ).removeClass( 'filter-selected' );
		$( this ).addClass( 'filter-selected' );
		
		if( $( this ).index() != 0 ) {
			$( ".header2" ).removeClass( "hidden" );		
		} else {
			$( ".header2" ).addClass( "hidden" );						
		}
		
		// Redisplay brunch on filter
		for ( var i in MM.Elements.storage ) {
            if ( MM.Elements.storage[ i ].db_id == MM.Elements.selected_branch_first_element_db_id ) {
                MM.Elements.storage[ i ].displayBranch( MM.Elements.storage[ i ], $( this ).index() );
				break;
            }			
        }
		
		MM.Elements.makeFirstTransperently();
				
	} );
	
	//email text
	$( '.email_input > input' ).on( 'click', function () {
		$( '.email_input > input' ).removeClass("email_error");
		if ( $( this ).val() == 'email' ) {
            $( this ).val( '' );
        }
	} )/*.on( 'focusout', function () {
		$( this ).val( 'email' );
	} );*/
	
	// Send Email
	$( ".send_email" ).click( function() {

	    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
			text = $( ".email_input > input" ).val();
		
	    if ( re.test( text ) ){
			
			$( ".email_step1" ).addClass( "hidden" );
			$( ".email_step2" ).removeClass( "hidden" );
				
			MM.Ajax.sendEmail( function () {
				setTimeout( function() {
				
					$( ".email_step1" ).removeClass( "hidden" );
					$( ".email_step2" ).addClass( "hidden" );
	
				}, 3000);
				$( '.email_input input' ).val( 'email' );
			});
		} else {			
			$( '.email_input > input' ).addClass("email_error");
		}


	} );
	
	// Get PDF
	$( ".save_as_pdf" ).click( function() {		
		MM.Ajax.getPDF( window.open("","_blank") );
	});
		
} );



