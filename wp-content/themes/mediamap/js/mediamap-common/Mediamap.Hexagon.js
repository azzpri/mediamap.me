window.MM = window.MM || { };

/**
 *  Hexagon
 */
MM.Hexagon = new function () {
	
	this.polygon = false;
	this.stroke = false;
	this.text = false;
	this.link = false;
	
	// Global onClick
	this.onClick = function () {
		MM.Canvas.move( MM.centerpoint, 1 );
		if ( MM.Elements.hasOwnProperty( 'selected_level' ) ) {
			MM.Elements.selected_level = 0;			
		}			
		MM.Elements.blurFirstElements( { 'opacity' : 1 } );
		MM.Elements.restoreColors();
	}
	
	// Draw Polygon
	this.drawPolygon = function ( center, sides, radius ) {
		this.polygon = new Path.RegularPolygon(center, sides, radius)
			.attach( 'click', function () {				
				MM.Hexagon.onClick();
			});
						
		this.polygon.fillColor = 'black';
		this.polygon.opacity = 0.01;
		
		this.stroke = new Path.RegularPolygon(center, sides, radius + 1 );
		this.stroke.strokeColor = "white";	
	}
	
	// Add Avatar
	this.addAvatar = function ( source ) {
						
		this.avatar = new Raster( {
			source : source,				
			crossOrigin: 'anonymous',
			position : [ MM.centerpoint.x , MM.centerpoint.y ],
			size: 20,				
		} ).attach( 'click', function () {									
			MM.Hexagon.onClick();	
		})
		
		this.avatar.onMouseEnter = function ( ) {
			$("html").css("cursor","pointer");
		}
		
		this.avatar.onMouseLeave = function ( ) {
			$("html").css("cursor","default");
		}
	}
	
	// Init
	this.init = function () {
			
		var center = MM.centerpoint;
		var sides = 6;
		var radius = 100;
		
		this.drawPolygon( center, sides, radius );
		
		//MM.background.on( 'click' , function(){
		//	MM.Elements.selected_level = 0;			
		//});
				
		var user_avatar = $(".user_avatar").html();		
				
		if ( user_avatar.length > 0 ) {			
			this.addAvatar( user_avatar );			
			
			var group = new Group( [ new Path.RegularPolygon( center, sides, radius - 3 ), this.avatar ] );
			group.clipped = true;

            return;
        }
		
		function addText ( content, point, fontSize ) {
			var text = new PointText( {
				point : point,
				content: content,
				fillColor: 'white',
				fontFamily: 'PT Sans',
				fontSize: fontSize,
			});
		
			text.bounds.x -= text.bounds.width/2;
			
			return text;
		}
		
		this.text = addText( "МЕДИЙНЫЕ", [ MM.centerpoint.x, MM.centerpoint.y - 25 ], '16px')
			.attach( 'click', function () {									
				MM.Hexagon.onClick();	
			});
		this.text2 = addText( "КОМПЕТЕНЦИИ", [ MM.centerpoint.x, MM.centerpoint.y - 6 ], '16px')
			.attach( 'click', function () {									
				MM.Hexagon.onClick();	
			});
		this.link = addText( "ПЕРСОНАЛИЗИРОВАТЬ", [ MM.centerpoint.x, MM.centerpoint.y +35 ], '12px')
			.attach( 'click', this.showAuth );
		
	}
	
	// Show Auth
	this.showAuth = function( event ) {
		if ( MM.Elements.selected_level == 0 ) {
			event.stopPropagation();
            $(".auth__popup").show();
        }
		
	}
	
	// calculate hex corner
	this.hex_corner = function ( center, size, i ) {
		var angle_deg = 60 * i   + 30;
		var angle_rad = Math.PI / 180 * angle_deg;
		return new Point( center.x + size * Math.cos( angle_rad ), center.y + size * Math.sin( angle_rad ) )
	}
}
