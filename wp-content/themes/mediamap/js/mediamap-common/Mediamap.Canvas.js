window.MM = window.MM || { };

/**
 * 	Canvas class
 */
MM.Canvas = new function () {

	this.currentZoom = this.initialZoom = 1;
	
	// Move canvas to some point
	this.move = function ( end_p, zoom, disableAnimation ) {
		
		if ( ! MM.isTablet() && typeof( disableAnimation ) === 'undefined' ) {				
			var start = Date.now(); // сохранить время начала
			
			var start_p = paper.view.center;
			
			var n_limit = 10;
			var time_interval = 40;
			
			var _x = ( end_p.x - start_p.x ) / n_limit;
			var _y = ( end_p.y - start_p.y ) / n_limit;
											
			var n = 1;
									
			var timer = setInterval(function() {		  
			
			  if ( n > n_limit ) {
				clearInterval(timer); // конец через 2 секунды
				
				MM.Canvas.zoom( zoom );
				
				return;
			  }
			
			  // рисует состояние анимации			  			  
			  paper.view.center = [ start_p.x + n * _x , start_p.y + n * _y ];
			  
			  n++;
			  
			}, time_interval );
		} else {
			paper.view.center = [ end_p.x, end_p.y ];
			MM.Canvas.zoom( zoom, true );
		}
	}

	// Zoom canvas	
	this.zoom = function ( zoom, disableAdimation ) {
				
		MM.Canvas.currentZoom = zoom;
		
		if ( ! MM.isTablet()  && typeof( disableAnimation ) === 'undefined'  ) {
			var n_limit = 10;
			var time_interval = 40;
			
			var start_zoom = paper.view.zoom;
			var x = ( zoom - paper.view.zoom ) / n_limit;
			var n = 1;
									
			var timer = setInterval(function() {		  
			
			  if ( n > n_limit ) {
				clearInterval(timer);
							
				paper.view.zoom = zoom;
				
				return;
			  }		  
			
			  // рисует состояние анимации			  			  
			  paper.view.zoom += x;	  
			  n++;
			  
			}, time_interval );
		} else {
			paper.view.zoom = zoom;
		}
		
	}
	
	this.branchCenter = function ( el ) {
					
		function getChildren( element, array ) {
			
			array.push( element );
			
			for( var i = 0; i < element.children.length; i++ ) {
				array = getChildren( element.children[ i ], array );  
			}
			
			return array;
        }
				
		var left = $("canvas").width();
		var bottom = 0;
		var right = 0;
		var top = $("canvas").height();		
		var elements = getChildren( el, [] );
		
		// if it level 1, check center. If > 1 add parent
		if (el.level == 1 ) {
            elements.push( { "Dot" : { "position": MM.centerpoint } } )
        } else {
			elements.push( el.parent )
		}		
		
		// Get maximum and minimum coordinates
		for( var i = 0; i < elements.length; i++ ) {
						
			var el_left = elements[ i ].Dot.position.x;
			var el_right = elements[ i ].Dot.position.x;
			var el_top = elements[ i ].Dot.position.y;
			var el_bottom = elements[ i ].Dot.position.y;
				
			if ( typeof( elements[ i ].Label ) !== 'undefined' ) {			
								
                if ( el_left > elements[ i ].Label.bounds.left )
					el_left = elements[ i ].Label.bounds.left;
				
				if ( el_right < elements[ i ].Label.bounds.right )
					el_right = elements[ i ].Label.bounds.right;
					
				if ( el_top > elements[ i ].Label.bounds.top )
					el_top = elements[ i ].Label.bounds.top;
					
				if ( el_bottom < elements[ i ].Label.bounds.bottom )
					el_bottom = elements[ i ].Label.bounds.bottom;		

            } 

			if ( el_left < left ){			
                left = el_left;
			}
			
			if ( el_right > right ){
                right = el_right;
			}
			
			if ( el_top < top ){		
                top = el_top;
			}
						
			if ( el_bottom > bottom ){		
                bottom = el_bottom;
			}						
		}
		
		left -= 20;
		right += 20;	
		bottom += 20;
		top -= 20;
		
		// remove headers height 
		if ( $( ".header1" ).length ) {
            top -= $( ".header1" ).height();
        }
		
		if ( $( ".header2" ).length ) {
			if ( $( ".header2" ).is(":visible") ) 
				top -= $( ".header2" ).height();
        }
		
		//Get zoom value
		var vx = Math.abs( paper.view.size.width / ( right - left ) );
		var vy = Math.abs( paper.view.size.height / ( bottom - top ) );
		
		var center_coordinates = [ (right + left) / 2 , (bottom + top) / 2  ];			
		
		if ( vx > vy ) {		
			var zoom_value = vy;				        
		} else {			
			var zoom_value = vx;					
		}						
		
		return {
			"point" : new Point( center_coordinates ),
			"value" : zoom_value*MM.Canvas.currentZoom
		} 
	}
	// resize Lables		
	this.countFontSize = function( size ) {
		
		if ( typeof( MM.Canvas.resize_ratio ) === 'undefined' ) {
			MM.Canvas.resize_ratio = paper.view.size.width / MM.base_screen_size;    
        }
		var new_size = size * MM.Canvas.resize_ratio;
        
		return new_size + 'px';
    }

}