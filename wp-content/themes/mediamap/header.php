<!doctype html>
<html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <!--meta property="og:url"           content="http://mediamap.me" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="MediaMap.me" />
	<meta property="og:description"   content="Персональная карта медийных компетенций" />
    <meta property="og:image"   content="http://mediamap.me/wp-content/themes/mediamap/images/background.jpg" /-->	
	<title><?php wp_title('«', true, 'right'); ?> <?php bloginfo('name'); ?></title>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/base.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>    
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/paper-full.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/Mediamap.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/Mediamap.Element.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/mediamap-common/Mediamap.common.js"></script>    
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/mediamap-common/Mediamap.Hexagon.js"></script>    
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/mediamap-common/Mediamap.Canvas.js"></script>
    <script type="text/javascript" src="http://vk.com/js/api/share.js?93" charset="windows-1251"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/index.js"></script>    
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter37201090 = new Ya.Metrika({
                        id:37201090,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true
                    });
                } catch(e) { }
            });
    
            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";
    
            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/37201090" style="position:absolute; left:-9999px;" alt="" /></div></noscript>    
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-77388029-1', 'auto');
      ga('send', 'pageview');
    
    </script>
    
    <?php include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); ?>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>